#!/usr/bin/env python
# Copyright (c) 2009 Technische Universitaet Muenchen, Informatik Lehrstuhl IX.
# Author: Federico Ruiz-Ugalde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


from numpy import array, amin, amax

from cmoc.robot.joint_controller import Controller_mass_sim, Controller_type
from arcospyu.yarp_tools.yarp_comm_helpers import ArcosYarp

def main():
    import yarp
    import sys
    import time
    from arcospyu.config_parser import ConfigFileParser
    config_parser=ConfigFileParser(sys.argv)
    options, args, config = config_parser.get_all()

    yarp.Network.init()

    arm_base_name=config.robotarm_portbasename
    module_base_name=arm_base_name+"/joint_sim"
    yarp_ctrl=ArcosYarp(ports_name_prefix=options.namespace, module_name_prefix=module_base_name)

    joints=config.nJoints

    port_base_name="/joint_sim"
    robotbn=options.namespace + config.robotarm_portbasename
    portbn=robotbn + port_base_name
    qvin_port=yarp_ctrl.create_yarp_port("/qvin", strict=False)
    qout_port=yarp_ctrl.create_yarp_port("/qout", input_port=False)
    qin_port=yarp_ctrl.create_yarp_port("/qin", strict=False)
    torque_in_port=yarp_ctrl.create_yarp_port("/torque_in", strict=False)
    sim_param_port=yarp_ctrl.create_yarp_port("/sim_param", strict=False)
    stiffness_port=yarp_ctrl.create_yarp_port(options.namespace + config.robotarm_portbasename+"/robot/stiffness", strict=False, full_name=True)
    #stiffnesspn=options.namespace + config.robotarm_portbasename+"/robot/stiffness"

    yconnect=yarp.Network.connect
    cstyle=yarp.ContactStyle()
    cstyle.persistent=True
    yarp_ctrl.connect(qout_port, arm_base_name+"/bridge", "/qin")
    yarp_ctrl.connect(qout_port, arm_base_name+"/bridge", "/qcmded")

    #yconnect(qoutpn, robotbn+"/bridge/qin", cstyle)
    #yconnect(qoutpn, robotbn+"/bridge/qcmded", cstyle)

    qv_max=config.max_vel_joint_sim
    kp_m=1.
    kp_m=2.
    kp=config.jpctrl_kp
    kvp=config.jpctrl_kvp
    inertia=0.005
    inertia=0.1
    inertia=config.jpctrl_inertia
    stiffness=array([kp*kvp]*joints)
    sim_param=[kp,kvp,inertia,stiffness]
    sim=Controller_mass_sim(kp,kvp,inertia,stiffness,joints,config.arm_extralimits, qv_max=qv_max)
    sim.set_initial_time(time.time())

    init_q=True
    while True:
        yarp_ctrl.update()

        stiffness_bottle=stiffness_port.read(False)
        if stiffness_bottle:
            stiffness=map(yarp.Value.asDouble,map(stiffness_bottle.get,range(stiffness_bottle.size())))
            sim_param[3]=stiffness
            sim.set_param(sim_param)

        sim_param_bottle=sim_param_port.read(False)
        if sim_param_bottle:
            sim_param=map(yarp.Value.asDouble,map(sim_param_bottle.get,range(sim_param_bottle.size())))
            sim.set_param(sim_param)

        qvin_bottle=qvin_port.read(False)
        if qvin_bottle:
            qvin=array(map(yarp.Value.asDouble,map(qvin_bottle.get,range(qvin_bottle.size()))))
            sim.set_controller(Controller_type.velocity)
            sim.set_qv_ref(qvin)
        qin_bottle=qin_port.read(False)
        if qin_bottle:
            qin=array(map(yarp.Value.asDouble,map(qin_bottle.get,range(qin_bottle.size()))))
            #sim.set_controller(Controller_type.position)
            sim.set_q(qin)
        if init_q:
            sim.set_q(config.initial_joint_pos)
            init_q=False
        torque_in_bottle=torque_in_port.read(False)
        if torque_in_bottle:
            torque_in=array(map(yarp.Value.asDouble,map(torque_in_bottle.get,range(torque_in_bottle.size()))))
            sim.set_ext_torque(torque_in)
        q=sim.get_q(time.time())
        sim.set_joint_limits(config.updateJntLimits(q))
        time.sleep(0.001)
        qout_bottle=qout_port.prepare()
        qout_bottle.clear()
        map(qout_bottle.addDouble,q)
        qout_port.write()

if __name__=="__main__":
    main()
