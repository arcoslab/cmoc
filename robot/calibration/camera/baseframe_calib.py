#!/usr/bin/env python

# This program takes an initial camera pose estimate and based on finger pose measurements and camera estimates, tries to improve the initial camera pose.
# It takes both measurements, computes a position distance and an angular distance. Then it changes each coordinate one by one (x, y, z) until distance error is minimal, then it changes each angle, until angular distance is minimal. Then it repeates, until error change is smaller than some small value. Then it stores this distance and angle corrections (with respect to the initial camera pose estimate.
# This is done for every measurement, then the mean of this corrections is taken as the final correction estimate. Then a final calibrated camera pose estimate is calculated.

#It calculates the expected camera position for every marker and finger pose. Then it calculates the mean of this expected camera position. That's the final camera position.

from numpy import identity, array, pi, dot, abs
from numpy.linalg import norm, inv
import sys
from cmoc.objects.sliding.utils import Object_pos_handle, finger_joint_move, move_robot
from arcospyu.robot_tools.robot_trans import rot_x, rot_y, rot_z, homo_matrix
from cmoc.robot.sim_handlers import Force_handle
from cmoc.robot.hand_sim_handler import Hand
import time
import PyKDL as kdl
import pickle
import optparse, os

from arcospyu.numeric.quat import from_matrix, to_matrix_fixed
from arcospyu.kdl_helpers.kdl_helpers import my_diff, my_adddelta

def finger_pose_to_finger_marker_pose(finger_pose, rel_pose_marker_finger):
    '''Returns the finger marker pose'''
    return(dot(finger_pose, rel_pose_marker_finger))

def get_real_camera_pose(cam_to_marker_pose, finger_cur_pose, rel_pose_marker_finger):
    '''Takes one measuremente and finds the improve direction, then it iterates until it finds the best value for this dimension'''
    finger_marker_cur_pose=finger_pose_to_finger_marker_pose(finger_cur_pose, rel_pose_marker_finger)
    real_camera_pose=dot(finger_marker_cur_pose, inv(cam_to_marker_pose))
    #print "Real camera pose", real_camera_pose
    real_camera_pose_q=[real_camera_pose[:3,3],from_matrix(real_camera_pose)]
    return(real_camera_pose_q)

def get_final_real_camera_pose(cam_to_marker_poses, finger_poses, rel_pose_marker_finger):
    real_camera_poses_q=[]
    #getting all poses
    for cam_to_marker_pose, finger_pose in zip(cam_to_marker_poses, finger_poses):
        real_camera_poses_q.append(get_real_camera_pose(cam_to_marker_pose, finger_pose, rel_pose_marker_finger))
    #calculating mean
    final_camera_pose_q=[array([0.]*3),array([0.]*4)]
    for camera_pose_q in real_camera_poses_q:
        #print "camera pose q", camera_pose_q
        #print "camera pose", to_matrix_fixed(camera_pose_q[1], r=camera_pose_q[0])
        final_camera_pose_q[0]+=camera_pose_q[0]
        final_camera_pose_q[1]+=camera_pose_q[1]
    final_camera_pose_q[0]/=len(real_camera_poses_q)
    final_camera_pose_q[1]/=len(real_camera_poses_q)
    final_camera_pose_q[1]/=norm(final_camera_pose_q[1])
    #print "final camera pose q", final_camera_pose_q
    return(to_matrix_fixed(final_camera_pose_q[1], r=final_camera_pose_q[0]))

    

class Calib(object):
    def __init__(self,webcam_finger_poses_filename, webcam_marker_filename, simulation, fcd, no_left, config_hands):
        self.config_hands=config_hands
        self.no_left=no_left
        self.fcd=fcd
        self.simulation=simulation
        self.prerecorded_poses=False
        self.record_poses=False
        self.calibrate=False
        self.num_object_measurements=30
        if webcam_finger_poses_filename!="":
            print "Executing prerecorded poses, creating markers datafile"
            self.prerecorded_poses=True
            self.webcam_finger_poses_filename=webcam_finger_poses_filename
        elif webcam_marker_filename=="":
            print "Recording new poses"
            self.record_poses=True
        else:
            print "Using markers file to calculate camera pose"
            self.webcam_marker_filename=webcam_marker_filename
            self.calibrate=True
            self.webcam_calib()
            


    def create_data(self):
        measured_finger_poses=[]
        camera_to_marker_poses=[]
        self.harm_bridge.cartesian_controller()
        first=True
        max_dist_camera_pose=0.1 #10 cm of max distance error between camera pose rough estimations
        max_angle_dist_camera_pose=10.*pi/180.
        for num_pose,cmd_finger_pose in enumerate(self.finger_poses):
            print "Moving to prerecorded position", num_pose, "of ", len(self.finger_poses), 100*num_pose/len(self.finger_poses), "%"
            move_robot(self.hand_right_server, self.finger, self.harm, cmd_finger_pose, [0.01, 1.*pi/180.])
            print "Waiting"
            time.sleep(2)
            print "Getting marker poses"
            camera_to_marker_pose=self.object_pos_handle.get_object_pos_mean(self.num_object_measurements,timeout=5.)
            if len(camera_to_marker_pose)==0:
                print "Timeout getting marker poses, continue to next pose"
                time.sleep(3)
                continue
            print "Getting finger poses"
            finger_data=array([0.]*10)
            for i in xrange(self.num_object_measurements):
                finger_data+=self.hforce.get_data_finger(self.finger)
            finger_data/=self.num_object_measurements
            finger_data[3:7]/=norm(finger_data[3:7])
            finger_pose=to_matrix_fixed(finger_data[3:7], r=finger_data[:3])
            # measured_finger_poses.append(array(self.harm.getPose()).reshape((4,4)))
            camera_pose=self.improve_camera_pose([camera_to_marker_pose], [finger_pose], identity(4))
            if first:
                last_correct_camera_pose=camera_pose
            else:
                dist_camera_pose=norm(camera_pose[:3,3]-last_correct_camera_pose[:3,3])
                angle_dist_camera_pose=norm(my_diff(camera_pose,last_correct_camera_pose,1)[3:])
                print "Distance between current and last correct camera poses", dist_camera_pose, angle_dist_camera_pose*180./pi
                if (dist_camera_pose>max_dist_camera_pose) or (angle_dist_camera_pose>max_angle_dist_camera_pose):
                    print "Current and previous camera poses are too different, may be a marker detection problem, skipping current pose"
                    time.sleep(3)
                    continue
                else:
                    last_correct_camera_pose=camera_pose
            print "Finger pose", finger_pose
            print "Marker to camera pose", camera_to_marker_pose
            print "Camera pose", camera_pose
            diffs, final_diff, max_dist_error, max_angle_error=self.calculate_marker_pose_errors([finger_pose], [camera_to_marker_pose], camera_pose, identity(4))
            print "Final diff", final_diff[:3], final_diff[3:]*180./pi
            print "Max dist error", max_dist_error
            print "Max angle error", max_angle_error*180./pi
            print "If error to big we have to discard this measurement"
            #raw_input()
            camera_to_marker_poses.append(camera_to_marker_pose)
            measured_finger_poses.append(finger_pose)
            first=False
        return(measured_finger_poses, camera_to_marker_poses)


    def calculate_marker_pose_errors(self, measured_finger_poses, camera_to_marker_poses, camera_pose, rel_pose_marker_finger):
        diffs=[]
        for measured_finger_pose, camera_to_marker_pose in zip(measured_finger_poses, camera_to_marker_poses):
        #for cmd_finger_pose in self.finger_poses:
        #    print "Moving to prerecorded position"
        #    move_robot(self.hand_right_server, self.finger, self.harm, cmd_finger_pose, [0.01, 1.*pi/180.])
        #    print "Waiting"
            #time.sleep(1)
        #    print "Getting finger and marker poses"
        #    self.measured_finger_poses.append(array(self.harm.getPose()).reshape((4,4)))
        #    self.camera_to_marker_poses.append(self.object_pos_handle.get_object_pos())
            from_camera_marker_pose=dot(camera_pose, camera_to_marker_pose)
            from_finger_marker_pose=dot(measured_finger_pose, rel_pose_marker_finger)
            #print "From camera marker pose", from_camera_marker_pose
            #print "From finger marker pose", from_finger_marker_pose
            diff=my_diff(from_camera_marker_pose, from_finger_marker_pose, 1.)
            #print "Diff", diff[:3], diff[3:]*180./pi
            diffs.append(diff)
            #raw_input("Press enter to continue")
        final_diff=0.
        max_dist_error=0.
        max_angle_error=0.
        for diff in diffs:
            final_diff+=abs(diff)
            if norm(diff[:3])>max_dist_error:
                max_dist_error=norm(diff[:3])
            if norm(diff[3:])>max_angle_error:
                max_angle_error=norm(diff[3:])
        final_diff/=len(diffs)
        print "Final diff", final_diff[:3], final_diff[3:]*180./pi
        print "Max dist error", max_dist_error
        print "Max angle error", max_angle_error*180./pi
        return(diffs, final_diff, max_dist_error, max_angle_error)


    def improve_camera_pose(self, camera_to_marker_poses, measured_finger_poses, rel_pose_marker_finger):
        print "Executing recorded poses"
        self.measured_finger_poses=[]
        self.camera_to_marker_poses=[]
        # for cmd_finger_pose in self.finger_poses:
        #     print "Moving to prerecorded position"
        #     move_robot(self.hand_right_server, self.finger, self.harm, cmd_finger_pose, [0.01, 5.*pi/180.])
        #     print "Waiting"
        #     #time.sleep(1)
        #     print "Getting finger and marker poses"
        #     self.measured_finger_poses.append(array(self.harm.getPose()).reshape((4,4)))
        #     self.camera_to_marker_poses.append(self.object_pos_handle.get_object_pos())
        #     #print "finger pose", self.measured_finger_poses[-1]
        #     #print "camera marker pose", self.camera_to_marker_poses[-1]
        #     #raw_input("Press enter to continue")
        final_camera_pose=get_final_real_camera_pose(camera_to_marker_poses, measured_finger_poses, rel_pose_marker_finger)
        print "Final camera pose", final_camera_pose
        return(final_camera_pose)


    def calib(self):
        if self.calibrate:
            sys.exit()

        # yarp ports
        base_name="/webcam_calib"
        self.base_name=base_name

        #Arm/finger control initialization.
        from vfclik.handlers import HandleArm, HandleJController, HandleBridge
        arm='right'
        hand='right'
        robot='lwr'
        fingers=[0,1,2,3]
        arm_portbasename="/"+robot+"/"+arm
        arm_portbasename_left="/"+robot+"/left"

        self.finger=2 #ring finger right hand
        finger_initial_pos=array([0.,10.,10.]) #angles in degrees
        thumb_finger_initial_pos=array([0.,20.,10.,10.]) #angles in degrees
        thumb_finger_pushing_pos=array([0.,20.,10.,10.]) #angles in degrees
        finger_pushing_pos=array([0.,20.,45.]) #angles in degrees

        initial_pose={
            'right': array([[-1., 0., 0., 0.8],
                            [0., 0., 1., -0.1],
                            [0., 1., 0., 1.14],
                            [0., 0., 0., 1.]]),
            'left': array([[-1., 0., 0., 0.74],
                           [0., 0., 1., -0.1],
                           [0., 1., 0., 1.14],
                           [0., 0., 0., 1.]])
            }

        webcal_initial_pose={
            'right': array([[0., 0., -1., 0.95],
                            [0., 1., 0., 0.0],
                            [1., 0., 0., 1.1],
                            [0., 0., 0., 1.]]),
            'left': array([[-1., 0., 0., 0.74],
                           [0., 0., 1., -0.1],
                           [0., 1., 0., 1.14],
                           [0., 0., 0., 1.]])
            }

        base_pushing_orient=array([[0.,0.,-1.], # Initial finger orientation. Or base finger transformation
                              [-1.,0.,0.],
                              [0.,1.,0.]])

        base_pushing_orient=array([[0.,0.,-1.], # Initial finger orientation. Or base finger transformation
                              [0.,1.,0.],
                              [1.,0.,0.]])
        #goal_precision=[0.01, 0.5*pi/180.0]
        goal_precision=[0.03, 3.0*pi/180.0]

        initial_joint_pos={
            "right": array([0.6, 0.91, 0.4, 1.6, 1.0, -0.95, 0.3]),
            "left": array([0.78, 1.6, -0.4, -1.3, 1, 0.5, 0.7])
            }

        cam_away_joint_pos={
            "right": array([0.6, 0.91, 0.4, 1.6, 1.0, -0.95, 0.3]),
            "left": array([-30., 45, -45, -70, -90., 90, 45])*pi/180.
            }

        #setting up
        self.harm=HandleArm(arm_portbasename,handlername=base_name+"arm")
        self.harm_joint=HandleJController(arm_portbasename,handlername=base_name+"arm_joint")
        self.harm_bridge=HandleBridge(arm_portbasename,handlername=base_name+"arm_bridge",torso=False)
        self.hand_right_server=Hand(self.config_hands, handedness="right",portprefix=base_name,sahand_number=0)
        if not self.no_left:
            self.lharm=HandleArm(arm_portbasename_left,handlername=base_name+"arm_left")
            self.lharm_joint=HandleJController(arm_portbasename_left,handlername=base_name+"arm_left_joint")
            self.lharm_bridge=HandleBridge(arm_portbasename_left,handlername=base_name+"arm_left_bridge",torso=False)
            self.hand_left_server=Hand(handedness="left",portprefix=base_name,sahand_number=1)

        self.hforce=Force_handle(base_name, "/torque_sim/force_out")

        if not self.simulation:
            print "Setting calibration offsets and factors"
            for finger,(angle_offset,torque_factor) in enumerate(zip(self.fcd.angle_calibration_data_right,self.fcd.torque_calibration_factors_right)):
                self.hand_right_server.fingers[finger].angle_offsets=angle_offset
                self.hand_right_server.fingers[finger].torque_calibration_factors=torque_factor
                print "Torque factor", self.hand_right_server.fingers[finger].torque_calibration_factors
                self.hand_right_server.update_angle_offsets(list_fingers=[finger])
            if not self.no_left:
                for finger,(angle_offset,torque_factor) in enumerate(zip(self.fcd.angle_calibration_data_left,self.fcd.torque_calibration_factors_left)):
                    self.hand_left_server.fingers[finger].angle_offsets=angle_offset
                    self.hand_left_server.fingers[finger].torque_calibration_factors=torque_factor
                    self.hand_left_server.update_angle_offsets(list_fingers=[finger])


        time.sleep(1)

        max_stiffness_finger=array([28.64,28.64,8.59])
        finger_max_speed=pi
        finger_speed=finger_max_speed/2.
        for i in fingers:
            self.hand_right_server.set_params(i,[finger_speed,finger_speed,finger_speed]+(max_stiffness_finger).tolist())
            if not self.no_left:
                self.hand_left_server.set_params(i,[finger_speed,finger_speed,finger_speed]+(max_stiffness_finger).tolist())
        if not self.no_left:
            self.hand_left_server.update_controller_params()
        self.hand_right_server.update_controller_params()
        #0.64 (soft for simulation) 40 (soft for real robot)
        #max_stiffness_arm=array([1000.0]*7)
        max_stiffness_arm=array([40.0]*7) #real arm soft
        max_stiffness_arm=array([240.0]*7) #a bit harder
        self.harm.set_stiffness(max_stiffness_arm)

        print "Initial finger joint pos"
        #Initial finger joint pos
        #finger_joint_move(self.hand_right_server, 0, thumb_finger_initial_pos, wait=-1, goal_precision=5.)
        finger_joint_move(self.hand_right_server, self.finger, finger_pushing_pos, wait=1, goal_precision=5.)

        #print "Arm away"
        #Send arm to away for looking for object position.
        #self.harm_bridge.joint_controller()
        #self.harm_joint.set_ref_js(cam_away_joint_pos["right"],wait=-1,goal_precision=[0.1]*7)
        #self.lharm_bridge.joint_controller()
        #self.lharm_joint.set_ref_js(cam_away_joint_pos["left"],wait=-1,goal_precision=[0.1]*7)

        #initial position
        self.harm_bridge.cartesian_controller()
        move_robot(self.hand_right_server, self.finger, self.harm, webcal_initial_pose["right"], [0.03, 5.*pi/180.])
        finger_joint_move(self.hand_right_server, 0, thumb_finger_pushing_pos, wait=1, goal_precision=5.)

        print "Ready for action"
        if self.prerecorded_poses:
            #Next run, we reproduce the recorded positions and generate the calibrated camera pose.
            self.record_marker_data()
        elif self.record_poses:
            #First we record to a file nice calibration positions using arm_keyboard_control
            self.record_positions()
        else:
            self.webcam_calib()

    def webcam_calib(self):
        rel_pose_marker_finger=identity(4)
        dist_error_threshold=0.001
        dist_error=dist_error_threshold*2.
        dist_error_change_threshold=0.000001
        dist_error_change=2*dist_error_change_threshold
        first=True
        while (dist_error_change>dist_error_change_threshold) and (dist_error>dist_error_threshold):
            rel_pose_marker_finger=self.correct_rel_pose_marker_finger_angles(rel_pose_marker_finger)
            rel_pose_marker_finger, dist_error, camera_pose=self.correct_rel_pose_marker_finger_dist(rel_pose_marker_finger)
            print "rel pose marker finger", rel_pose_marker_finger
            print "Camera pose", camera_pose
            print "Distance error", dist_error
            if first:
                last_dist_error=dist_error
            else:
                dist_error_change=abs(last_dist_error-dist_error)
                last_dist_error=dist_error
            print "dist error change", dist_error_change
            #time.sleep(3)
            #raw_input("Another main iteration")
            first=False
        print "camera_pose=", repr(camera_pose)
        print "rel_pose_marker_finger=", repr(rel_pose_marker_finger)


    def correct_rel_pose_marker_finger_dist(self, initial_rel_pose_marker_finger):
        print "Correcting by changing marker translation"
        #time.sleep(3)
        rel_pose_marker_finger=initial_rel_pose_marker_finger
        stop_error=0.001 #cm
        stop_angle_error=0.001*pi/180.
        stop_distance_error=0.001
        error=stop_error*2

        base_dist_step=0.1
        dist_step=base_dist_step
        base_angle_step=5.*pi/180.0
        angle_step=base_angle_step

        webcam_marker_file=open(self.webcam_marker_filename)
        measured_finger_poses, camera_to_marker_poses=pickle.load(webcam_marker_file)
        webcam_marker_file.close()

        cur_axis=0 #0: x, 1: y, 2: z
        cont=True
        first=True
        cur_x_dist=0.
        cur_y_dist=0.
        cur_z_dist=0.
        cur_dir=1. #positive
        check_improvement=False
        improved=True
        angle_error_change_threshold=0.001*pi/180.0
        dist_error_change_threshold=0.00000001
        dist_error_change_threshold_iter=0.001
        last_iter_dist_error=10.
        stop_correction=False
        #camera_pose=self.improve_camera_pose(camera_to_marker_poses, measured_finger_poses, rel_pose_marker_finger)
        while cont:
            #estimate camera pose
            camera_pose=self.improve_camera_pose(camera_to_marker_poses, measured_finger_poses, rel_pose_marker_finger)

            #raw_input("Press enter to check errors")
            #calculate errors
            diffs, final_diff, max_dist_error, max_angle_error=self.calculate_marker_pose_errors(measured_finger_poses, camera_to_marker_poses, camera_pose, rel_pose_marker_finger)
            # if angle errors are still big, then create a rotation matrix first in one angle in one direction. In the next iteration check if the error in this angle was reduced, if not, then create a rotation matrix in the same angle but in another direction, in the next iteration check if the error is reduced, if not, reduced the size step, and try everything again, but in some point we have to stop here or try with next angle. If the error is reduced then rotate again in the same direction again, with the same step.
            if first:
                old_max_angle_error=max_angle_error
                old_max_dist_error=max_dist_error
                previous_rel_pose_marker_finger=rel_pose_marker_finger
            if max_dist_error>stop_distance_error:
#            if max_angle_error>stop_angle_error:
                print "dist error still big, correcting"
                if check_improvement:
                    print "Checking for improvement"
                    #if max_angle_error<old_max_angle_error:
                    if max_dist_error<old_max_dist_error:
                        print "Improved"
                        improved=True
                        correct_last_rel_pose_marker_finger=rel_pose_marker_finger
                        previous_rel_pose_marker_finger=rel_pose_marker_finger
                    else:
                        improved=False
                        print "Worse, use rel_finger_marker_pose from last time"
                        correct_last_rel_pose_marker_finger=previous_rel_pose_marker_finger
                        #max_dist_error=old_max_dist_error
                if not improved:
                    print "Changing direction because of no improvement"
                    cur_dir*=-1.
                    dist_step/=2.
                if improved:
                    if not first:
                        angle_error_change=abs(max_angle_error-old_max_angle_error)
                        print "Angle Error change", angle_error_change*180.0/pi
                        dist_error_change=abs(max_dist_error-old_max_dist_error)
                        print "Distance Error change", dist_error_change
#                        if angle_error_change<angle_error_change_threshold: #or to much iterations
                        if dist_error_change<dist_error_change_threshold: #or to much iterations
                            #print "Angle error smaller than threshold, stopping correction for this angle"
                            print "Distance error smaller than threshold, stopping correction for this angle"
                            stop_correction=True
                            cur_axis+=1
                            if cur_axis==3:
                                print "All axis ready"
                                iter_dist_error_change=abs(max_dist_error-last_iter_dist_error)
                                last_iter_dist_error=max_dist_error
                                print "Iteration error change", iter_dist_error_change
                                if iter_dist_error_change>dist_error_change_threshold_iter:
                                    print "Do another iteration"
                                    cur_axis=0
                                #cont=False
                                    #raw_input()
                                else:
                                    print "No more iterations"
                                    cur_axis=0
                                    cont=False
                                    #raw_input()
                if not stop_correction:
                    if first:
                        correct_last_rel_pose_marker_finger=rel_pose_marker_finger
                    if cur_axis==0:
                        print "Correcting dist x"
                    #rotate x by angle step in currrent direction
                        new_x_dist=cur_x_dist+dist_step*cur_dir
                        print "Translating in", cur_axis, "by", new_x_dist
                        rot=homo_matrix(rot_m=identity(3), trans=array([new_x_dist,0.,0.]))
                        rel_pose_marker_finger=dot(correct_last_rel_pose_marker_finger, rot)
                        check_improvement=True
                    if cur_axis==1:
                        print "Correcting dist y"
                    #rotate x by angle step in currrent direction
                        new_y_dist=cur_y_dist+dist_step*cur_dir
                        print "Translating in", cur_axis, "by", new_y_dist
                        rot=homo_matrix(rot_m=identity(3), trans=array([0., new_y_dist,0.]))
                        rel_pose_marker_finger=dot(correct_last_rel_pose_marker_finger, rot)
                        check_improvement=True
                    elif cur_axis==2:
                        print "Correcting dist z"
                    #rotate z by angle step in currrent direction
                        new_z_dist=cur_z_dist+dist_step*cur_dir
                        print "Rotating in", cur_axis, "by", new_z_dist
                        rot=homo_matrix(rot_m=identity(3), trans=array([0., 0., new_z_dist]))
                        rel_pose_marker_finger=dot(correct_last_rel_pose_marker_finger, rot)
                        check_improvement=True
                else:
                    print "Trying with next dist"
                    improved=True
                    cur_dir=1.
                    stop_correction=False
                    dist_step=base_dist_step
            else:
                cont=False
            if not improved:
                old_max_angle_error=old_max_angle_error
                old_max_dist_error=old_max_dist_error
            else:
                old_max_angle_error=max_angle_error
                old_max_dist_error=max_dist_error
            first=False
            #raw_input()

        print "Marker finger relative pose", rel_pose_marker_finger
        #raw_input()
        return(rel_pose_marker_finger, max_dist_error, camera_pose)


    def correct_rel_pose_marker_finger_angles(self, initial_rel_pose_marker_finger):
        print "Correcting by changing marker angles"
        #time.sleep(1)
        rel_pose_marker_finger=initial_rel_pose_marker_finger
        stop_error=0.001 #cm
        stop_angle_error=0.001*pi/180.
        stop_distance_error=0.001
        error=stop_error*2

        dist_step=0.0005
        base_angle_step=5.*pi/180.0
        angle_step=base_angle_step

        webcam_marker_file=open(self.webcam_marker_filename)
        measured_finger_poses, camera_to_marker_poses=pickle.load(webcam_marker_file)
        webcam_marker_file.close()

        cur_rot=0 #0: x, 1: y, 2: z
        cont=True
        first=True
        cur_x_angle=0.
        cur_y_angle=0.
        cur_z_angle=0.
        cur_dir=1. #positive
        check_improvement=False
        improved=True
        angle_error_change_threshold=0.001*pi/180.0
        dist_error_change_threshold=0.00000001
        dist_error_change_threshold_iter=0.001
        last_iter_dist_error=10.
        stop_correction=False
        #camera_pose=self.improve_camera_pose(camera_to_marker_poses, measured_finger_poses, rel_pose_marker_finger)
        while cont:
            #estimate camera pose
            camera_pose=self.improve_camera_pose(camera_to_marker_poses, measured_finger_poses, rel_pose_marker_finger)

            #raw_input("Press enter to check errors")
            #calculate errors
            diffs, final_diff, max_dist_error, max_angle_error=self.calculate_marker_pose_errors(measured_finger_poses, camera_to_marker_poses, camera_pose, rel_pose_marker_finger)
            # if angle errors are still big, then create a rotation matrix first in one angle in one direction. In the next iteration check if the error in this angle was reduced, if not, then create a rotation matrix in the same angle but in another direction, in the next iteration check if the error is reduced, if not, reduced the size step, and try everything again, but in some point we have to stop here or try with next angle. If the error is reduced then rotate again in the same direction again, with the same step.
            if first:
                old_max_angle_error=max_angle_error
                old_max_dist_error=max_dist_error
                previous_rel_pose_marker_finger=rel_pose_marker_finger
            if max_dist_error>stop_distance_error:
#            if max_angle_error>stop_angle_error:
                print "angle error still big, creating rotation matrix"
                if check_improvement:
                    print "Checking for improvement"
                    print "max dist error", max_dist_error, "old max dist error", old_max_dist_error
                    #if max_angle_error<old_max_angle_error:
                    if max_dist_error<old_max_dist_error:
                        print "Improved"
                        improved=True
                        correct_last_rel_pose_marker_finger=rel_pose_marker_finger
                        previous_rel_pose_marker_finger=rel_pose_marker_finger
                    else:
                        improved=False
                        print "Worse, use rel_finger_marker_pose from last time"
                        correct_last_rel_pose_marker_finger=previous_rel_pose_marker_finger
                        #old_max_dist_error=old_max_dist_error
                if not improved:
                    print "Changing direction because of no improvement"
                    cur_dir*=-1.
                    angle_step/=2.
                if improved:
                    if not first:
                        angle_error_change=abs(max_angle_error-old_max_angle_error)
                        print "Angle Error change", angle_error_change*180.0/pi
                        dist_error_change=abs(max_dist_error-old_max_dist_error)
                        print "Distance Error change", dist_error_change
#                        if angle_error_change<angle_error_change_threshold: #or to much iterations
                        if dist_error_change<dist_error_change_threshold: #or to much iterations
                            #print "Angle error smaller than threshold, stopping correction for this angle"
                            print "Distance error smaller than threshold, stopping correction for this angle"
                            stop_correction=True
                            cur_rot+=1
                            if cur_rot==3:
                                print "All angles ready, try again"
                                iter_dist_error_change=abs(max_dist_error-last_iter_dist_error)
                                last_iter_dist_error=max_dist_error
                                print "Iteration error change", iter_dist_error_change
                                if iter_dist_error_change>dist_error_change_threshold_iter:
                                    print "Do another iteration"
                                    cur_rot=0
                                #cont=False
                                    #raw_input()
                                else:
                                    print "No more iterations"
                                    cur_rot=0
                                    cont=False
                                    #raw_input()
                if not stop_correction:
                    if first:
                        correct_last_rel_pose_marker_finger=rel_pose_marker_finger
                    if cur_rot==0:
                        print "Correcting angle x"
                    #rotate x by angle step in currrent direction
                        new_x_angle=cur_x_angle+angle_step*cur_dir
                        print "Rotating in", cur_rot, "by", new_x_angle*180.0/pi
                        rot=homo_matrix(rot_m=rot_x(new_x_angle))
                        rel_pose_marker_finger=dot(correct_last_rel_pose_marker_finger, rot)
                        check_improvement=True
                    if cur_rot==1:
                        print "Correcting angle y"
                    #rotate x by angle step in currrent direction
                        new_y_angle=cur_y_angle+angle_step*cur_dir
                        print "Rotating in", cur_rot, "by", new_y_angle*180./pi
                        rot=homo_matrix(rot_m=rot_y(new_y_angle))
                        rel_pose_marker_finger=dot(correct_last_rel_pose_marker_finger, rot)
                        check_improvement=True
                    elif cur_rot==2:
                        print "Correcting angle z"
                    #rotate z by angle step in currrent direction
                        new_z_angle=cur_z_angle+angle_step*cur_dir
                        print "Rotating in", cur_rot, "by", new_z_angle*180./pi
                        rot=homo_matrix(rot_m=rot_z(new_z_angle))
                        rel_pose_marker_finger=dot(correct_last_rel_pose_marker_finger, rot)
                        check_improvement=True
                else:
                    print "Trying with next angle"
                    improved=True
                    cur_dir=1.
                    stop_correction=False
                    angle_step=base_angle_step
            else:
                cont=False
            if not improved:
                old_max_angle_error=old_max_angle_error
                old_max_dist_error=old_max_dist_error
            else:
                old_max_angle_error=max_angle_error
                old_max_dist_error=max_dist_error
            first=False
            #raw_input()

        print "Marker finger relative pose", rel_pose_marker_finger
        #raw_input()
        return(rel_pose_marker_finger)

    def record_marker_data(self):
        print "Starting marker recording"
        raw_input()
        webcam_poses_file=open(self.webcam_finger_poses_filename)
        self.finger_poses=pickle.load(webcam_poses_file)
        webcam_poses_file.close()
        sim_model=self.simulation # When using model simulation, otherwise object position from marker tracking
        self.object_pos_handle=Object_pos_handle(self.base_name, simulation=sim_model)
        measured_finger_poses, camera_to_marker_poses=self.create_data()
        webcam_marker_filename="webcam_marker"+"_"+time.strftime("%d.%m.%Y_%H.%M.%S")+".dat"
        webcam_marker_file=open(webcam_marker_filename,"wb")
        pickle.dump([measured_finger_poses, camera_to_marker_poses], webcam_marker_file)
        webcam_marker_file.close()


    def record_positions(self):
        cont=True
        self.finger_poses=[]
        while(cont):
            cmd=raw_input("Press enter to get a new position, or press q to finish")
            if cmd=="q":
                break
                cont=False
            finger_data=self.hforce.get_data_finger(self.finger)
            finger_pose=to_matrix_fixed(finger_data[3:7], r=finger_data[:3])
            #finger_pose=array(self.harm.getPose()).reshape((4,4))
            print "finger_cart", finger_pose[:3,3]
            self.finger_poses.append(finger_pose)
        webcam_poses_filename="webcam_poses"+"_"+time.strftime("%d.%m.%Y_%H.%M.%S")+".dat"
        webcam_poses_file=open(webcam_poses_filename,"wb")
        pickle.dump(self.finger_poses, webcam_poses_file)
        webcam_poses_file.close()



def main():
    parser=optparse.OptionParser("usage: %prog [options]")
    parser.add_option("-p", "--poses_file", dest="poses_filename", default="",
help="webcam finger poses filename")
    parser.add_option("-m", "--marker_file", dest="marker_filename", default="",
help="webcam finger poses filename")
    parser.add_option("-c", "--finger_cal_data", dest="finger_cal_data_filename", default="robot_description/arcosbot/kinematics/sahand/calibration_data/finger_calibration_data.py", type="string", help="Finger calibration data filename")
    parser.add_option("-f", "--config_hands", dest="config_hands", default="robot_description/arcosbot/kinematics/sahand/hands_kin.py", type="string", help="hands config filename")
    parser.add_option("-s", "--simulation", action="store_true", dest="sim", default=False,help="Simulation")
    parser.add_option("-r", "--no-left", dest="no_left", action="store_true", default=False, help="Disable left arm")
    (options, args)= parser.parse_args(sys.argv[1:])
    from arcospyu.config_parser.config_parser import import_config
    fcd=import_config(options.finger_cal_data_filename)
    config_hands=import_config(options.config_hands)
    webcam_finger_poses_filename=options.poses_filename
    webcam_marker_filename=options.marker_filename
    calib=Calib(webcam_finger_poses_filename, webcam_marker_filename, options.sim, fcd, options.no_left, config_hands)
    calib.calib()

if __name__=="__main__":
    main()
