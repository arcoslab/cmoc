#!/usr/bin/env python

import roslib
roslib.load_manifest('tf')
roslib.load_manifest('rospy')
import rospy
rospy.init_node("ros_to_yarp_box_pose")
import tf

import sys
from utils import Filter_vector, Filter
sys.path.append("../../control/motionControl")
sys.path.append("../../tools/python/numeric/")
sys.path.append("../../perception/webcam_baseframe_calibration/")
from arcospyu.yarp_tools.yarp_comm_helpers import new_port, write_narray_port
from arcospyu.control.control_loop import Controlloop
from pyrovito.pyrovito_utils import Roboviewer_objects
from numpy import concatenate, array, dot, identity
from numpy.linalg import norm, inv
from arcospyu.numeric import quat
from webcam_calibration_values import camera_pose
from object_params import Object_params

class Filtered_marker(object):
    def __init__(self, tfl, base_frame, marker_frame, filter_order=3, filter_freq=0.7):
        self.filter=Filter_vector(dim=7, order=filter_order, freq=filter_freq)
        self.tfl=tfl
        self.marker_frame=marker_frame
        self.base_frame=base_frame
        self.min_freq=5.
        self.max_period=1./self.min_freq
        self.old_time=rospy.get_time()

    def get_marker(self):
        valid_marker=False
        while not valid_marker:
            marker_ct=self.tfl.getLatestCommonTime(self.base_frame, self.marker_frame)
            self.time=rospy.get_time()
            if self.time-marker_ct.to_sec()>self.max_period:
                print "Too old data, no more markers visible"
            else:
                valid_marker=True
        trans, orient=self.tfl.lookupTransform(self.base_frame, self.marker_frame, rospy.Time())
        print "Trans", trans, orient
        data_out=self.filter.filter(concatenate((array(trans), array(orient))))
        data_out[3:7]/=norm(data_out[3:7])

        return(data_out)

class Control(Controlloop):
    def set_params(self,params=[]):
        #Configuration params
        print "Params"
        use_calib=params["use_calib"]
        self.object_params=Object_params()
        self.box_dim=self.object_params.box_dim
        self.markers_transformations={ #this matrices represent what's the pose of the marker with respect of the center of the object
            "/4x4_3": array([[1., 0., 0., 0.],
                             [0., 1., 0., 0.],
                             [0., 0., 1., 0.],
                             [0., 0., 0., 1.]])
            }
        self.markers_transformations.update(self.object_params.markers_transformations)
        self.object_markers=self.object_params.markers_transformations.keys()
        print "Using markers", self.object_markers
        if use_calib:
            self.camera_transformation=camera_pose
            print "using calibration"
            raw_input()
        else:
            self.camera_transformation=array([[1., 0., 0., 0.],
                                              [0., 1., 0., 0.],
                                              [0., 0., 1., 0.],
                                              [0., 0., 0., 1.]])

        base_frame="world"
        self.marker_frame="/4x4_42"
        self.marker_frame="ar_marker"
        base_portname="/marker"
        out_portname="/object_pos:o"

        #ros tf
        self.tfl=tf.TransformListener()
        self.tfl_markers=tf.Transformer(cache_time=rospy.Duration(10))
        #marker filter
        self.filtered_markers=Filtered_marker(self.tfl, base_frame, self.marker_frame)
        #yarp
        print "TESt1"
        self.out_port=new_port(base_portname+out_portname, "in", "none", timeout=0.)
        print "TEST2"

        #visualization
        self.view_objects=Roboviewer_objects(base_portname,"/lwr/roboviewer",counter=3040)
        self.box_id=self.view_objects.create_object("frame")
        self.view_objects.send_prop(self.box_id,"scale",[0.1,0.1,0.4])
        self.view_objects.send_prop(self.box_id,"timeout",[-1])
        #self.view_objects.send_prop(self.goal_id,"color",[0,1,0])
        box_vis=identity(4)
        self.view_objects.send_prop(self.box_id,"pose",
                                    box_vis.reshape(16).tolist())


    def process(self):
        try:
            data=self.filtered_markers.get_marker()
        except tf.Exception:
            print "Marker transformation not possible"
        else:
            print "Data", data
            data_m=quat.to_matrix_fixed(data[3:7], r=data[:3])
            data_out=dot(self.camera_transformation,dot(data_m, inv(self.markers_transformations[self.marker_frame])))
            self.view_objects.send_prop(self.box_id,"pose", data_out.reshape(16).tolist())
            write_narray_port(self.out_port, data_out.reshape(16))

import optparse, os

def main():
    parser=optparse.OptionParser("usage: %prog [options]")
    #parser.add_option("-c", "--calibration_file", dest="cal_filename", default="", help="webcam camera calibration filename")
    parser.add_option("-c", "--use_calibration", action="store_true", dest="use_calib", default=False,help="Use calibration file")
    (options, args)= parser.parse_args(sys.argv[1:])
    control_loop=Control(15.)
    control_loop.loop(use_calib=options.use_calib)


if __name__=="__main__":
    main()
