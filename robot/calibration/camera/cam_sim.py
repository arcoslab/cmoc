#!/usr/bin/env python

#This program read current finger position and applies a camera transformation
#to this to simulate a camera position and adds some gausian noise.
#Then it transmits this relative position as a marker position.

import sys
from arcospyu.robot_tools.robot_trans import rot_z, homo_matrix, rot_y, rot_x, add_noise_to_pose
from vfclik.handlers import HandleArm, HandleJController, HandleBridge
from arcospyu.yarp_tools.yarp_comm_helpers import new_port, readListPort, write_narray_port
from numpy import array, dot, pi, identity
from numpy.linalg import inv
from pyrovito.pyrovito_utils import Roboviewer_objects
import time

def main():
    arcos_namespace="/0"
    base_name="/slider_sim"
    arm='right'
    hand='right'
    robot='lwr'
    fingers=[0,1,2,3]
    arm_portbasename="/"+robot+"/"+arm
    harm=HandleArm(arcos_namespace+arm_portbasename,handlername=arcos_namespace+base_name+"/arm")
    marker_pose_out_port=new_port(base_name+"/xo:out", "out", "none", timeout=1.)
    camera_pose=array([[1., 0., 0., 0.1],
                       [0., 1., 0., 0.6],
                       [0., 0., 1., 0.2],
                       [0., 0., 0., 1.]])

    camera_rot=rot_z(60.*pi/180.)
    camera_rot=dot(rot_x(24.*pi/180.), camera_rot)
    camera_rot=dot(rot_y(35.*pi/180.), camera_rot)
    camera_pose=array([[1., 0., 0., 0.1],
                       [0., 1., 0., 0.6],
                       [0., 0., 1., 0.2],
                       [0., 0., 0., 1.]])

    camera_rot=rot_z(60.*pi/180.)
    camera_rot=dot(rot_x(24.*pi/180.), camera_rot)
    camera_rot=dot(rot_y(35.*pi/180.), camera_rot)
    camera_pose=dot(camera_pose, homo_matrix(rot_m=camera_rot))
    camera_pose=identity(4)
    #camera_pose=dot(camera_rot, camera_pose)
    #camera_rot=homo_matrix(rot_m=rot_y(20.*pi/180.))
    #camera_pose=dot(camera_rot, camera_pose)
    print "Camera pose", camera_pose
    finger_marker_rot=rot_z(15.*pi/180.)
    finger_marker_rot=dot(rot_x(24.*pi/180.), finger_marker_rot)
    finger_marker_rot=dot(rot_y(35.*pi/180.), finger_marker_rot)
    finger_to_marker_offset=homo_matrix(rot_m=finger_marker_rot, trans=array([0.05,0.,0.]))
    finger_to_marker_offset=identity(4)
    #finger_to_marker_offset=homo_matrix(rot_m=rot_z(0.*pi/180.0), trans=array([0.0,0.,0.]))
    print "Finger to marker pose", finger_to_marker_offset

    #visualization
    print "Sending visualization data"
    view_objects=Roboviewer_objects(arcos_namespace+base_name,arcos_namespace
                                    +"/lwr/roboviewer", wait_connection=True)
    time.sleep(0.5)
    box_id=view_objects.create_object("frame")
    time.sleep(0.5)
    view_objects.send_prop(box_id,"scale",[0.05,0.05,0.1])
    time.sleep(0.5)
    print "This is a slow process because pyrovito is ineficient"
    view_objects.send_prop(box_id,"timeout",[-1])
    #view_objects.send_prop(goal_id,"color",[0,1,0])
    box_vis=identity(4)
    time.sleep(0.5)
    view_objects.send_prop(box_id,"pose",
                                box_vis.reshape(16).tolist())
    print "Starting loop"


    while True:
        #read current finger pose
        finger_pose=array(harm.getPose(blocking=True)).reshape((4,4))
        marker_pose=dot(finger_pose, finger_to_marker_offset)
        #apply camera transformation
        camera_to_marker_pose=dot(inv(camera_pose), marker_pose)
        #put noise to marker pose
        #print "Camera marker pose", camera_to_marker_pose
        camera_to_marker_pose_noise=add_noise_to_pose(camera_to_marker_pose, dist_noise=0.01, angle_noise=1.0*pi/180.0)
        #print "Camera marker pose noise", camera_to_marker_pose_noise
        #raw_input()
        #send relative marker pose
        view_objects.send_prop(box_id,"pose", camera_to_marker_pose_noise.reshape(16).tolist())
        write_narray_port(marker_pose_out_port, camera_to_marker_pose_noise.reshape(16))
        #write_narray_port(marker_pose_out_port, camera_to_marker_pose.reshape(16))

if __name__=="__main__":
    main()
