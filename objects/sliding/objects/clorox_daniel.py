#!/usr/bin/env python
#Object parameters file.
from numpy import array, identity, pi
from cmoc.objects.sliding import LScircular

class Object_params(object):
    def __init__(self):
                #model parameters
        #self.cylinder_dim=[0.094, 0.094, 0.23]
        self.cylinder_dim=[0.05, 0.05, .06]
        self.cylinder_center=array([0.,0.,0])
        self.cylinder_planes=[[(1.,0.,0),(self.cylinder_dim[0]/2.,0.,0.)], #(normal vector to face, center point of the face)
                        [(-1.,0.,0),(-self.cylinder_dim[0]/2.,0.,0.)],
                        [(0.,1.,0),(0.,self.cylinder_dim[1]/2.,0.)],
                        [(0.,-1.,0),(0,-self.cylinder_dim[1]/2.,0.)],
                        [(0.,0.,1),(0.,0.,self.cylinder_dim[2]/2.)],
                        [(0.,0.,-1),(0.,0.,-self.cylinder_dim[2]/2.)]]
       
        self.weight_balance=0.268
        self.weight=0.24127960284 #kilos
        self.rotational_inertia=self.weight*(self.cylinder_dim[0]**2)/2
        self.weight_force=9.81*self.weight
        self.friction_finger_z_force=0.
        self.friction_coef_object_table_static=0.342754805732
        self.friction_coef_finger_object_static=0.937802992473
        self.friction_coef_object_table=0.0292487204877
        self.friction_coef_finger_object=0.660347225488
        self.fmax=self.friction_coef_object_table*(self.weight_force-self.friction_finger_z_force)
        self.mmax_base=LScircular.calc_mmax_analytic(self.friction_coef_object_table,1.,self.cylinder_dim[0]*2.)
        self.mmax=self.mmax_base*(self.weight_force-self.friction_finger_z_force)
        print "fmax, mmax", self.fmax, self.mmax

        self.max_tilt_angle=3.0*pi/180.
        self.tilt_height=self.cylinder_dim[2]/2.-0.3*self.cylinder_dim[2]
        self.markers_transformations={ #this matrices represent what's the pose of the marker with respect of the center of the object
            "/4x4_1": array([[0., 0., -1., -self.cylinder_dim[0]/2.],
                             [-1., 0., 0., 0.02],
                             [0., 1., 0., 0.033+0.035/2.],
                             [0., 0., 0., 1.]])
#            "/4x4_1": identity(4)
            }
