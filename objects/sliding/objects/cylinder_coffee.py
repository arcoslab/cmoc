#!/usr/bin/env python
#Object parameters file.
from numpy import array, identity, pi
from cmoc.objects.sliding import LS

class Object_params(object):
    def __init__(self):
        #model parameters
        #self.box_dim=[0.094, 0.094, 0.23]
        self.box_dim=[0.106, 0.106, 0.205] #thickness, width, height
        self.box_center=array([0.,0.,0])
        self.box_planes=[[(1.,0.,0),(self.box_dim[0]/2.,0.,0.)], #(normal, point)
                         [(-1.,0.,0),(-self.box_dim[0]/2.,0.,0.)],
                         [(0.,1.,0),(0.,self.box_dim[1]/2.,0.)],
                         [(0.,-1.,0),(0,-self.box_dim[1]/2.,0.)],
                         [(0.,0.,1),(0.,0.,self.box_dim[2]/2.)],
                         [(0.,0.,-1),(0.,0.,-self.box_dim[2]/2.)]]
        self.box_vertices={
            0: array([[self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2]]),
            1: array([[-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2]]),
            2: array([[-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2]]),
            3: array([[self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2]]),
            4: array([[-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2]]),
            5: array([[-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2]])}
        self.weight_balance=1.034
        self.weight=0.732925953955 #kilos
        self.gravity_force=9.78 #Costa Rica
        self.weight_force=self.gravity_force*self.weight
        self.friction_finger_z_force=0.
        self.friction_coef_object_table_static=1.09751461822
        self.friction_coef_finger_object_static=0.497805838303
        self.friction_coef_object_table=0.70379556268
        self.friction_coef_finger_object=0.464006119762
        self.fmax=self.friction_coef_object_table*(self.weight_force-self.friction_finger_z_force)
        self.mmax_base=LS.calc_mmax_analytic(self.friction_coef_object_table,1.,self.box_dim[0],self.box_dim[1])
        self.mmax=self.mmax_base*(self.weight_force-self.friction_finger_z_force)
        print "fmax, mmax", self.fmax, self.mmax

        self.max_tilt_angle=3.0*pi/180.
        self.tilt_height=self.box_dim[2]/2.-0.1*self.box_dim[2]
        self.marker_height=0.261   #from object base
        self.markers_transformations={ #this matrices represent what's the pose of the marker with respect of the center of the object
            "ar_marker": array([[0., 0., -1., 0.0], #real box offset
                                [-1., 0., 0., 0.008], # strange y offset
                                [0., 1., 0., (self.marker_height-(self.box_dim[2]/2.0))], #real box offset
                                [0., 0., 0., 1.]])
            }

# 2019.11.15 17:00
# Object finger:
# Static friction coefficient 0.520374932635
# Dynamic friction coefficient 0.547254204094
# Static friction coefficient std 0.0876306558794
# Dynamic friction coefficient std 0.0424808603542
# Object table:
# Static friction coefficient 1.09751461822
# Dynamic friction coefficient 0.70379556268
# Static friction coefficient std 0.0939051683142
# Dynamic friction coefficient std 0.0383840585308
# Weight 0.732925953955
#2019.11.15 17:10
# Object finger:
# Static friction coefficient 0.497805838303   * Better than the previous
# Dynamic friction coefficient 0.464006119762   * Better then the previous
# Static friction coefficient std 0.111776005716
# Dynamic friction coefficient std 0.0360262342741
# Object table:
# Static friction coefficient 0.903581642252
# Dynamic friction coefficient 0.71906931933
# Static friction coefficient std 0.0
# Dynamic friction coefficient std 0.0
# Weight 0.803475176559

