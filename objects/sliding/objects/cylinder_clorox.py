#!/usr/bin/env python
#Object parameters file.
from numpy import array, identity, pi
from cmoc.objects.sliding import LS

class Object_params(object):
    def __init__(self):
        #model parameters
        #self.box_dim=[0.094, 0.094, 0.23]
        self.box_dim=[0.084, 0.084, 0.215] #thickness, width, height
        self.box_center=array([0.,0.,0])
        self.box_planes=[[(1.,0.,0),(self.box_dim[0]/2.,0.,0.)], #(normal, point)
                         [(-1.,0.,0),(-self.box_dim[0]/2.,0.,0.)],
                         [(0.,1.,0),(0.,self.box_dim[1]/2.,0.)],
                         [(0.,-1.,0),(0,-self.box_dim[1]/2.,0.)],
                         [(0.,0.,1),(0.,0.,self.box_dim[2]/2.)],
                         [(0.,0.,-1),(0.,0.,-self.box_dim[2]/2.)]]
        self.box_vertices={
            0: array([[self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2]]),
            1: array([[-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2]]),
            2: array([[-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2]]),
            3: array([[self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2]]),
            4: array([[-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2]]),
            5: array([[-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2]])}
        self.weight_balance=0.649
        self.weight=0.600580880823 #kilos
        self.gravity_force=9.78 #Costa Rica
        self.weight_force=self.gravity_force*self.weight
        self.friction_finger_z_force=0.
        self.friction_coef_object_table_static=0.418605317243
        self.friction_coef_finger_object_static=0.413488217033
        self.friction_coef_object_table=0.297773435689
        self.friction_coef_finger_object=0.337945961497
        self.fmax=self.friction_coef_object_table*(self.weight_force-self.friction_finger_z_force)
        self.mmax_base=LS.calc_mmax_analytic(self.friction_coef_object_table,1.,self.box_dim[0],self.box_dim[1])
        self.mmax=self.mmax_base*(self.weight_force-self.friction_finger_z_force)
        print "fmax, mmax", self.fmax, self.mmax

        self.max_tilt_angle=3.0*pi/180.
        self.tilt_height=self.box_dim[2]/2.-0.1*self.box_dim[2]
        self.marker_height=0.274   #from object base
        self.markers_transformations={ #this matrices represent what's the pose of the marker with respect of the center of the object
            "ar_marker": array([[0., 0., -1., 0.0], #real box offset
                                [-1., 0., 0., 0.008], # strange y offset
                                [0., 1., 0., (self.marker_height-(self.box_dim[2]/2.0))], #real box offset
                                [0., 0., 0., 1.]])
            }

# Object finger:
# Static friction coefficient 0.424484428439
# Dynamic friction coefficient 0.374811317173
# Static friction coefficient std 0.0465704409832
# Dynamic friction coefficient std 0.0403472712758
# Object table:
# Static friction coefficient 0.394310665079
# Dynamic friction coefficient 0.254377718417
# Static friction coefficient std 0.020201257444
# Dynamic friction coefficient std 0.018465783153
# Weight 0.647947217149

# #2019.11.15, 4:23pm
# Object finger:
# Static friction coefficient 0.413488217033
# Dynamic friction coefficient 0.337945961497
# Static friction coefficient std 0.0455853858259
# Dynamic friction coefficient std 0.0385908279785
# Object table:
# Static friction coefficient 0.418605317243
# Dynamic friction coefficient 0.297773435689
# Static friction coefficient std 0.0337082168593
# Dynamic friction coefficient std 0.0185126810061
# Weight 0.600580880823

