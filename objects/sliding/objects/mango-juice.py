#!/usr/bin/env python
#Object parameters file.
from numpy import array, identity, pi
from cmoc.objects.sliding import LS

class Object_params(object):
    def __init__(self):
        #model parameters
        #self.box_dim=[0.094, 0.094, 0.23]
        self.box_dim=[0.065, 0.097, 0.18] #thickness, width, height
        self.box_center=array([0.,0.,0])
        self.box_planes=[[(1.,0.,0),(self.box_dim[0]/2.,0.,0.)], #(normal, point)
                         [(-1.,0.,0),(-self.box_dim[0]/2.,0.,0.)],
                         [(0.,1.,0),(0.,self.box_dim[1]/2.,0.)],
                         [(0.,-1.,0),(0,-self.box_dim[1]/2.,0.)],
                         [(0.,0.,1),(0.,0.,self.box_dim[2]/2.)],
                         [(0.,0.,-1),(0.,0.,-self.box_dim[2]/2.)]]
        self.box_vertices={
            0: array([[self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2]]),
            1: array([[-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2]]),
            2: array([[-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2]]),
            3: array([[self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2]]),
            4: array([[-self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2],
                      [-self.box_dim[0]/2,self.box_dim[1]/2,self.box_dim[2]/2]]),
            5: array([[-self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,self.box_dim[1]/2,-self.box_dim[2]/2],
                      [self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2],
                      [-self.box_dim[0]/2,-self.box_dim[1]/2,-self.box_dim[2]/2]])}
        self.weight_balance=1.110 #kilos
        self.weight=1.06612657502 #kilos (with tilting)
        self.gravity_force=9.78 #Costa Rica
        self.weight_force=self.gravity_force*self.weight
        self.friction_finger_z_force=0.
        self.friction_coef_object_table_static=0.532985991117
        self.friction_coef_finger_object_static=0.637590541694
        self.friction_coef_object_table=0.459767136311
        self.friction_coef_finger_object= 0.589476378527
        self.fmax=self.friction_coef_object_table*(self.weight_force-self.friction_finger_z_force)
        self.mmax_base=LS.calc_mmax_analytic(self.friction_coef_object_table,1.,self.box_dim[0],self.box_dim[1])
        self.mmax=self.mmax_base*(self.weight_force-self.friction_finger_z_force)
        print "fmax, mmax", self.fmax, self.mmax

        self.max_tilt_angle=3.0*pi/180.
        self.tilt_height=self.box_dim[2]/2.-0.1*self.box_dim[2]
        self.markers_transformations={ #this matrices represent what's the pose of the marker with respect of the center of the object
            "ar_marker": array([[0., 0., -1., -self.box_dim[0]/2.], #real box offset
                                [-1., 0., 0., 0.008], # strange y offset
                                [0., 1., 0., (0.119-(self.box_dim[2]/2.0))], #real box offset
                                [0., 0., 0., 1.]])
#            "/4x4_1": identity(4)
            }
        self.markers_transformations={ #this matrices represent what's the pose of the marker with respect of the center of the object
            "ar_marker": array([[0., 0., -1., -self.box_dim[0]/2.], #real box offset
                                [-1., 0., 0., 0.013], # strange y offset
                                [0., 1., 0., (0.119-(self.box_dim[2]/2.0))], #real box offset
                                [0., 0., 0., 1.]])
#            "/4x4_1": identity(4)
            }
        self.markers_transformations={ #this matrices represent what's the pose of the marker with respect of the center of the object
            "ar_marker": array([[0., 0., -1., -self.box_dim[0]/2.], #real box offset
                                [-1., 0., 0., 0.005], # strange y offset
                                [0., 1., 0., (0.119-(self.box_dim[2]/2.0))], #real box offset
                                [0., 0., 0., 1.]])
#            "/4x4_1": identity(4)
            }

#2019.11.15
# Object finger:
# Static friction coefficient 0.637590541694
# Dynamic friction coefficient 0.589476378527
# Static friction coefficient std 0.0210557346665
# Dynamic friction coefficient std 0.0245126808535
# Object table:
# Static friction coefficient 0.532985991117
# Dynamic friction coefficient 0.459767136311
# Static friction coefficient std 0.0345109817156
# Dynamic friction coefficient std 0.0373580047247
# Weight 1.06612657502
