#!/usr/bin/env python
# Copyright (c) 2013 Federico Ruiz Ugalde
# Authors: Federico Ruiz Ugalde <memeruiz at gmail.com>, Daniel Garcia Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from numpy import array
from arcospyu.robot_tools.robot_trans import homo_matrix
from numpy.linalg import norm, inv, pinv
from arcospyu.kdl_helpers.kdl_helpers import rot_vector_angle
from numpy import dot, arctan, cross, sqrt, concatenate, pi, cos


'''
Returns the matrix A.
maps the forces (fx, fy, m) to the center velocity (direction)
'''
def forces_to_no_map(fmax,mmax):
    A=array([[2/fmax**2,0,0], # maps forces to nc
             [0,2/fmax**2,0],
             [0,0,2/mmax**2]])
    return(A)

'''
Returns matrix B_inv
maps the velocity in the pint of contact
to the force in the point of contact
'''
def nc_to_forces_map(fmax,mmax,contact_local #point of contact [x,y]
):
    a=(4*(mmax**2/fmax**2+contact_local[1]**2)*(mmax**2/fmax**2+contact_local[0]**2)
       -4*contact_local[0]**2*contact_local[1]**2)
    B_inv=(2./a)*array( #maps nc to forces
        [[mmax**2/fmax**2+contact_local[0]**2,contact_local[0]*contact_local[1]],
         [contact_local[0]*contact_local[1]  ,mmax**2/fmax**2+contact_local[1]**2]])
    return(B_inv)

'''
Returns matrix C
maps the force in the point of contact to the force in the center
'''
def force_c_to_force_o_map(contact_local):
    C=array([[1,0], #maps elipsoid to contact point
             [0,1],
             [-contact_local[1],contact_local[0]]])
    return(C)


##planar sliding simple

'''
returns m matrix
maps forces in point of contact to velocity in point of contact
'''
def fc_to_nc_matrix(fmax,mmax,contact_pos):
    m=2*array([[mmax**2/fmax**2+contact_pos[1]**2,-contact_pos[0]*contact_pos[1]],
               [-contact_pos[0]*contact_pos[1],mmax**2/fmax**2+contact_pos[0]**2]])
    return(m)

### planar sliding

'''

'''
def norm_nc_under_vc(fmax,mmax,contact_pos,fc2):
    nc=mmax*sqrt(1/( (mmax**2/fmax**2+contact_pos[1]**2)*fc2[0]**2 +
                     (mmax**2/fmax**2+contact_pos[0]**2)*fc2[1]**2 -
                     2*contact_pos[0]*contact_pos[1]*fc2[0]*fc2[1]))
    return(nc)

def norm_Vc_to_norm_Vo(contact_pos_local,cor_local,Vc):
    norm_rccor=norm(contact_pos_local[:2]-cor_local[:2])
    norm_rocor=norm(-cor_local[:2])
    norm_Vo=norm(Vc)*norm_rocor/norm_rccor
    return(norm_Vo)


def forces_to_nc_map(fmax,mmax,contact_local):
    B=2*array([[mmax**2/fmax**2+contact_local[1]**2,-contact_local[0]*contact_local[1]],
               [-contact_local[0]*contact_local[1],mmax**2/fmax**2+contact_local[0]**2]])
    return(B)


def vo_to_vc(vo_local,contact_pos_local,fmax,mmax):
    A=forces_to_no_map(fmax,mmax)
    B_inv=nc_to_forces_map(fmax,mmax,contact_pos_local)
    C=force_c_to_force_o_map(contact_pos_local)

    #system calculation
    P=dot(A,dot(C,B_inv))
    P_inv=pinv(P)
    vc_local=dot(P_inv,vo_local)/mmax**2
    return(vc_local)


def vo_to_vc_map(contact_local):
    D=array([[1,0,-contact_local[1]],
             [0,1,contact_local[0]]])
    return(D)

def ellipse_in_c(mmax,fmax,contact_local,fc_local):
    one=(1/mmax**2)*((mmax**2/fmax**2+contact_local[1]**2)*fc_local[0]**2+
                     (mmax**2/fmax**2+contact_local[0]**2)*fc_local[1]**2-
                     2*contact_local[0]*contact_local[1]*fc_local[0]*fc_local[1])
    print "Ellipse in c", one

def nc_max_min(table_face_normal,finger_face_normal,
               friction_coef_finger_object,LC_c_matrix):
    #rotation is over the table_face_normal_axis
    #We rotate finger_face_normal by arctan(friction_coef) to obtain corresponding
    #forces.
    #We apply the this forces through B to obtain nc_max_min
    rot_mat=rot_vector_angle(-table_face_normal,
                             arctan(friction_coef_finger_object))
    fc_max=dot(rot_mat,-finger_face_normal)
    nc_max=concatenate((dot(LC_c_matrix,fc_max[:2]),array([0.])))
    nc_max/=norm(nc_max)
    rot_mat=rot_vector_angle(-table_face_normal,
                             -arctan(friction_coef_finger_object))
    fc_min=dot(rot_mat,-finger_face_normal)
    nc_min=concatenate((dot(LC_c_matrix,fc_min[:2]),array([0.])))
    nc_min/=norm(nc_min)
    #print "fc max,min", fc_max, fc_min
    return(nc_max,nc_min,fc_max,fc_min)

def vc_from_vfinger(vfinger,finger_pos, box_planes, finger_face_normal_cuadratic,
                    table_face_normal, friction_coef_finger_object,fmax,mmax):
    #FIXME: calculation for sliding.
    #TODO radius hardcoded
    finger_face_normal=array([finger_pos[0]/0.5, finger_pos[1]/0.5, 0.0])
    print "normal----", norm(finger_face_normal)
    LC_c_matrix=nc_to_forces_map(fmax,mmax,finger_pos)
    nc_max,nc_min,fc_max,fc_min=nc_max_min(table_face_normal, finger_face_normal,
                             friction_coef_finger_object,inv(LC_c_matrix)) #TODO change to non inverse
    vfinger=concatenate((vfinger[:2],array([0.])))
    vfingera=array(vfinger)
    vfingera/=norm(vfingera)
    print "Nc max, min", nc_max, nc_min, vfingera
    w1=dot(cross(nc_min,vfingera),-table_face_normal)
    w2=dot(cross(nc_max,vfingera),-table_face_normal)
    if w1<0:
        #Vfinger smaller than nc_mix, finger slides to the right
        vc=nc_min*dot(vfinger,-finger_face_normal)/\
            dot(nc_min,-finger_face_normal) #projection of vfinger on nc_min that
                                            #is parallel to finger_face_normal
        print "Slide right"
        result=1
    elif w2>0:
        #Vfinger bigger than nc_max, finger slides to the left
        vc=nc_max*dot(vfinger,-finger_face_normal)/\
            dot(nc_max,-finger_face_normal)
        print "Slide left"
        result=-1
    else:
        #finger not sliding
        vc=vfinger
        result=0
    #vc=vfinger
    return(vc,result)

def vc_to_vo_linear_model(vc_local,contact_pos_local,fmax,mmax):
    A=forces_to_no_map(fmax,mmax)
    B_inv=nc_to_forces_map(fmax,mmax,contact_pos_local)
    C=force_c_to_force_o_map(contact_pos_local)

    #system calculation
    fc_temp=dot(B_inv,vc_local[:2]) #this force has an incorrect magnitude
    fo_temp=dot(C,fc_temp) #this force has an incorrect magnitude
    vo_local=dot(A,fo_temp) #this velocity has an incorrect magnitude
    vo_local*=mmax**2 #correcting velocity magnitude

    #Internal system step values
    nc_under_vc=norm_nc_under_vc(fmax,mmax, contact_pos_local, fc_temp)
    fc_local=concatenate((fc_temp[:2],array([0.])))* nc_under_vc
    fo_local=fo_temp*nc_under_vc

    return(vo_local,fc_local, fo_local)


def find_finger_object_face(vfinger,pos_finger,radius=0.5, height=0.3):
    #vfinger,pos_finger,radius
    #TODO take into account height
    touch=False
    finger_distance_to_axis=sqrt(pos_finger[0]**2+ pos_finger[1]**2)
    print (finger_distance_to_axis-radius)
    #compute 2 dimensional dot product to have angle
    cos_angle_vel_pos=(pos_finger[0]*vfinger[0]+pos_finger[1]*vfinger[1])/sqrt((pos_finger[0]**2+pos_finger[1]**2)*(vfinger[0]**2+vfinger[1]**2))

    if  cos_angle_vel_pos>0:
        print "AWAY"
    else:
        print "TO_CENTER"
    if finger_distance_to_axis<radius and pos_finger[2]<height:
        touch=True
        if  cos_angle_vel_pos>0:
            #going away from cylinder
            #print "AWAY", angle_vel_pos, pi
            touch=False
        if vfinger[2]>0.0:
            touch=True
            print "vertical--vertical"
        #if velocity less radial inner magnitud
        #touch = False
    else:
        touch=False
    return (touch, 5)

#Added by Daniel for inertia
'''
Returns a transformation matrix
maps velocity in center to center forces, using Limit Surface aproximation
'''
def vo_to_forces(vo_local, #velocity of center
                 fmax, mmax):
    scaling_matrix=array([[fmax,0.,0.],
                          [0.,fmax,0.],
                          [0.,0.,mmax]])

    new_v=dot(scaling_matrix, vo_local)
    k=1.0/(norm(new_v))
    k1=fmax**2*k
    k2=fmax**2*k
    k3=mmax**2*k
    transformation_matrix=array([[k1,0.,0.],
                                [0.,k2,0.],
                                [0.,0.,k3]])
    return transformation_matrix

'''
Returns a transformation matrix
maps forces in center to acceleration in center
'''
def forces_to_accelerations(object_mass, object_rotational_inertia):
    transformation_matrix=array([[1.0/object_mass,0.,0.],
                                 [0.,1.0/object_mass,0.],
                                 [0.,0.,1.0/object_rotational_inertia]])
    return transformation_matrix

'''
Returns the object acceleration calculated from object velocity
'''
def free_movement_acceleration(vo_local, #object velocity 
                               fmax, mmax, object_mass, object_rotational_inertia):

    T_v_f=vo_to_forces(vo_local, fmax, mmax)
    forces=dot(T_v_f, vo_local)
    M_inv=forces_to_accelerations(object_mass, object_rotational_inertia)
    acceleration=dot(M_inv, forces)
    return acceleration
    
