#!/usr/bin/python
#created by daniel 8-nov-2015

from scipy.integrate import dblquad
from numpy import sqrt, exp, e, power
from numpy import arange, pi, sin, cos, tan, array, arctan2, arctan, dot, arcsinh
from numpy.linalg import norm
import matplotlib.pyplot as plt
from multiprocessing import Pool
from mpl_toolkits.mplot3d import Axes3D
import time,os

#first define the functions of force depending on rotation center
fay=lambda u,p,cx,cy,x,y: u*p*(x-cx)/sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy))
fax=lambda u,p,cx,cy,x,y: u*p*(cy-y)/sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy))

#force in x 
def fx(l1l,l1h,cx,cy,u,p,eps=0.00001):
    fax2=lambda y,x: fax(u,p,cx,cy,x,y)
    #return(dblquad(fax2,l1l,l1h,lambda x: l2l, lambda x: l2h,epsabs=eps, epsrel=eps))
    return(dblquad(fax2,l1l,l1h,lambda x: -sqrt(((l1h-l1l)/2)*((l1h-l1l)/2)-x*x), lambda x: sqrt(((l1h-l1l)/2)*((l1h-l1l)/2)-x*x),epsabs=eps, epsrel=eps))

#force in y
def fy(l1l,l1h,cx,cy,u,p,eps=0.00001):
    fay2=lambda y,x: fay(u,p,cx,cy,x,y)
    #return(dblquad(fay2,l1l,l1h,lambda x: l2l, lambda x: l2h,epsabs=eps, epsrel=eps))
    return(dblquad(fay2,l1l,l1h,lambda x: -sqrt(((l1h-l1l)/2)*((l1h-l1l)/2)-x*x), lambda x: sqrt(((l1h-l1l)/2)*((l1h-l1l)/2)-x*x),epsabs=eps, epsrel=eps))

#torque
def m(l1l,l1h,cx,cy,u,p,eps=0.00001):
    fay2=lambda x,y: fay(u,p,cx,cy,x,y)
    fax2=lambda x,y: fax(u,p,cx,cy,x,y)
    m=lambda y,x: x*fay2(x,y)-y*fax2(x,y)
    #return(dblquad(m,l1l,l1h,lambda x:l2l, lambda x:l2h,epsabs=eps, epsrel=eps))
    return(dblquad(m,l1l,l1h,lambda x: -sqrt(((l1h-l1l)/2)*((l1h-l1l)/2)-x*x), lambda x: sqrt(((l1h-l1l)/2)*((l1h-l1l)/2)-x*x),epsabs=eps, epsrel=eps))

#returns coordinates of a limit surface point, taking into account, x force, y force, and torque.
def LS_point((l1l,l1h, hole_radius,cx,cy,u,p),eps=0.1):
    r=hole_radius #notation simplicity
    #hole computations
    center=(l1l+l1h)*0.5
    h1l=center-r
    h1h=center+r
    hole_force_x=fx(h1l,h1h,cx,cy,u,p,eps=eps)[0]
    hole_force_y=fy(h1l,h1h,cx,cy,u,p,eps=eps)[0]
    hole_torque=m(h1l,h1h,cx,cy,u,p,eps=eps)[0]
    
    #external computations
    ext_force_x=fx(l1l,l1h,cx,cy,u,p,eps=eps)[0]
    ext_force_y=fy(l1l,l1h,cx,cy,u,p,eps=eps)[0]
    ext_torque=m(l1l,l1h,cx,cy,u,p,eps=eps)[0]
    
    return(( ext_force_x-hole_force_x ,ext_force_y-hole_force_y , ext_torque-hole_torque ))

#returns max traslational force
def calc_fmax(u,fn):
    return(u*fn)

#returns max traslational force, but with integral
def calc_fmax_integral(u, fn,diameter=1, hole_radius=0):
    cx=1/0.0001
    cy=0.0001
    area=pi*((diameter**2)/4-hole_radius**2)
    p=fn/area
    return fy(-diameter/2, diameter/2, cx, cy, u, p)[0]-fy(-hole_radius, hole_radius, cx, cy, u, p)[0]

#returns max torque
def calc_mmax(u,fn,diameter, hole_radius=0):
    cx=0.0001
    cy=0.0001
    area=pi*((diameter**2)/4-hole_radius**2)
    p=fn/area
    torque=m(-diameter/2.,diameter/2.,cx,cy,u,p,eps=0.0001)[0]-m(-hole_radius,hole_radius,cx,cy,u,p,eps=0.0001)[0]
    return [torque, 0]
    

#returns max torque using the analytic way.
def calc_mmax_analytic(u,fn,diameter):
    mmax=u*fn*diameter/3.
    return(mmax)


#returns rotation matrix in Y, parameter is angle in radians
def Rz(angle):
    return(array([[cos(angle),-sin(angle),0.0],
                  [sin(angle),cos(angle),0.0],
                  [0.0       ,0.0       ,1.0]]))

#returns rotation matrix in X, parameter is angle in radians
def Rx(angle):
    return(array([[1.,        0.,         0.],
                  [0.,cos(angle),-sin(angle)],
                  [0.,sin(angle),cos(angle)]]))

#Create the Limit surface using the integrals
def create_data(angle_steps_dir,angle_steps_rad,l1l,l1h, hole_radius,u,p):
    pool=Pool()
    arguments=[]
    diameter=l1h-l1l
    for a in arange(0.0,2*pi,2.*pi/(angle_steps_dir)): #cor direction
        for b in arange(pi/2./angle_steps_rad,pi/2.,pi/2./angle_steps_rad): #cor radius
            cx=(((l1h-l1l)/2.)*cos(a)/tan(b))+(l1h-l1l)/2.+l1l
            cy=((l1h-l1l)/2.)*sin(a)/tan(b)+(l1h-l1l)/2.+l1l
            arguments.append((l1l,l1h, hole_radius ,cx,cy,u,p))
            #arguments.append((l1,l2,-cx,cy,u,den))
            
    points=pool.map(LS_point,arguments)
    print points[0]
    return(points) #points is a list of the limit surface points

#returns an aproximated limit surface point, using ellipsoid equation
def ellipsoid_points((Fx,Fy, mmax, fmax)):
    a=1-(Fx/fmax)**2-(Fy/fmax)**2
    if a>=0:
        Fz=mmax*sqrt(1-(Fx/fmax)**2-(Fy/fmax)**2)
        return((Fx,Fy,Fz))
    else:
        return (0,0,0)

#create points for the limit surface aproximation
def create_data_ellipsoid(angle_steps_dir,angle_steps_rad, mmax, fmax):
    p=Pool()
    arguments=[]

    for a in arange(0.0,2*pi,2.*pi/(angle_steps_dir)): #cor direction
        for b in arange(0.000001,pi/2.,pi/2./angle_steps_rad): #cor radius
            Fx=cos(a)*cos(b)
            Fy=sin(a)*cos(b)
            arguments.append((Fx,Fy, mmax, fmax))

    points=p.map(ellipsoid_points,arguments)

    return(points)

#returns an aproximated limit surface point, using superellipsoid equation
def superellipsoid_points((Fx,Fy,n1,n2,n3)):
    Fz=(1.-((abs(Fx))**n1+(abs(Fy))**n2))**(1/n3)
    return((Fx,Fy,Fz))

#create points for the limit surface aproximation (using superellipsoid)
def create_data_superellipsoid(angle_steps_dir,angle_steps_rad):
    p=Pool()
    arguments=[]

    for a in arange(0.0,2*pi,2.*pi/(angle_steps_dir)): #cor direction
        for b in arange(0.000001,pi/2.,pi/2./angle_steps_rad): #cor radius
            Fx=cos(a)*cos(b)
            Fy=sin(a)*cos(b)
            arguments.append((Fx,Fy,2.,3.3,1.1)) 
    points=p.map(superellipsoid_points,arguments)
    return(points)

def main():

    #object properties
    diameter=1.
    l1=1. #lado 1
    l2=1. #lado 2
    hole_radius=0.49
    u=1.
    fn=1.
    area=pi*((diameter**2)/4-hole_radius**2)
    p=fn/area
    fmax=calc_fmax(u,fn)
    fmax2=calc_fmax_integral(u, fn, diameter, hole_radius)
    mmax=calc_mmax(u, fn, diameter, hole_radius)[0]
    mmax2=calc_mmax_analytic(u, fn, diameter)
    print "Fmax, Mmax", fmax, fmax2, mmax, mmax2

    #generation steps
    steps=3.1
    steps_dir=40
    steps_rad=27.1
    steps_rad=15.1

    #plot stuff
    plt.ion()
    fig=plt.figure()
    colors=['r','g','b','c', 'm', 'y', 'k', 'w']
    color_n=0
    plots=[]
    ax=Axes3D(fig)
    ax.set_xlabel("Fx")
    ax.set_ylabel("Fy")
    ax.set_zlabel("m")
    
    lss=[]

    #plot ellipsoid
    lss=create_data_ellipsoid(steps_dir,steps_rad, mmax, fmax2)
    a,b,c=zip(*lss)
    wframe=ax.scatter(a,b,c,'z',c='g')
    plt.draw()
    raw_input()
    #ax.collections.remove(wframe)
    wframe = None
    
    lss=create_data(steps_dir,steps_rad,-l1/2,l1/2, hole_radius,u,p)
    a,b,c=zip(*lss)
    color=colors[color_n]
    color_n+=1
    if color_n==len(colors):
        color_n=0
    oldcol = wframe
    color='b'
    wframe=ax.scatter(a,b,c,'z',c=color)
    
    if oldcol is not None:
        ax.collections.remove(oldcol)
    
    plt.draw()
        
    raw_input()
    

if __name__=="__main__":
    main()


