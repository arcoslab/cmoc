#!/usr/bin/python
# Copyright (c) 2018 Universidad de Costa Rica
# Author: Daniel Garcia Vaglio
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import yarp
from numpy import array, identity, pi
from time import time, sleep
from pyrovito.pyrovito_utils import Roboviewer_objects
from cmoc.robot.hand_sim_handler import Hand
from vfclik.handlers import HandleArm, HandleBridge
from arcospyu.kdl_helpers import my_diff, my_adddelta
from cmoc.objects.sliding.utils import move_robot, finger_joint_move
from arcospyu.config_parser.config_parser import import_config
from arcospyu.rawkey.rawkey import Raw_key, Keys
from os.path import expanduser

import argparse


class RobotController():
    def __init__(self, ignore_oms=False, namespace="", acceleration=0.01,
                 damping=0.0):
        # Import the hand configuration
        self.config_hands = import_config(
            expanduser(
                "".join(
                    [
                        "~/local/src/robot_descriptions/arcosbot/",
                        "kinematics/sahand/hands_kin.py"
                    ]
                )
            )
        )
        self.ignore_oms = ignore_oms
        self.acceleration = acceleration
        self.damping = damping

        # Initialize the Hnadlers
        arm_portbasename = namespace + "/lwr/right"
        base_name = "/robot_controller"
        self.harm = HandleArm(arm_portbasename, handlername=base_name + "/arm")
        self.harm_bridge = HandleBridge(
            arm_portbasename,
            handlername=(base_name + "/arm_bridge"),
            torso=False)
        self.hand = Hand(
            self.config_hands,
            handedness="right",
            arcoslab_namespace=namespace,
            portprefix="/robot_controller",
            sahand_port_name_prefix="/sahand",
            sahand_number=0)

        # Visualization
        self.view_objects = Roboviewer_objects(
            base_name, namespace+"/lwr/roboviewer", counter=300)

        # Connection to OMS
        local_cmd_name = "/robot/cmd"
        remote_cmd_name = "/oms/robot/cmd"
        self.cmd_port = yarp.BufferedPortBottle()
        self.cmd_port.open(local_cmd_name)

        local_pose_name = "/robot/pose"
        remote_pose_name = "/oms/robot/pose"
        self.pose_port = yarp.BufferedPortBottle()
        self.pose_port.open(local_pose_name)

        local_twist_name = "/robot/twist"
        remote_twist_name = "/oms/robot/twist"
        self.twist_port = yarp.BufferedPortBottle()
        self.twist_port.open(local_twist_name)
        sleep(0.05)  # One should give some time to yarp

        yarp.Network.connect(remote_cmd_name, local_cmd_name, "tcp")
        yarp.Network.connect(local_pose_name, remote_pose_name, "tcp")
        yarp.Network.connect(local_twist_name, remote_twist_name, "tcp")
        sleep(0.05)  # Again, give yarp some time

        self.previous_pose = identity(4)
        self.first = True
        self.push_finger = 3  # Using ring finger for pushing
        self.vel = array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0])
        self.raw_key = Raw_key()

    def initial_configuration(self):
        # Set arm stiffness
        max_stiffness_arm = array([240.0]*7)
        self.harm.set_stiffness(max_stiffness_arm)

        # Set the cartesian controller
        self.harm_bridge.cartesian_controller()

        thumb_pushing_pos = array([0.0, 5.03322, 50.9657, 7.74433])

        finger_pushing_pos = array([-1.2259, 25.6364, 25.52])

        # Set the finger parameters
        finger_speed = [pi / 2.0] * 3
        finger_max_stiffness = [28.64, 28.64, 8.59]
        self.hand.set_params(
            self.push_finger, finger_speed + finger_max_stiffness)
        self.hand.update_controller_params()

        # Move thumb to desired position
        finger_joint_move(self.hand, 0,
                          thumb_pushing_pos, wait=0,
                          goal_precision=5.)

        # Move fingers to initial position
        for finger in range(1,3):
            finger_joint_move(self.hand, finger, array([0.0, 10.0, 10.0]),
                              wait=10., goal_precision=5.)

        finger_joint_move(
            self.hand,
            self.push_finger,
            finger_pushing_pos,
            wait=10,
            goal_precision=5.)


        # Adding the goal to the visualization
        self.goal_id = self.view_objects.create_object("frame")
        self.view_objects.send_prop(self.goal_id, "scale", [0.1, 0.1, 0.1])
        self.view_objects.send_prop(self.goal_id, "timeout", [-1])


        # Adding the table to the visualization
        table_pose = identity(4)
        table_pose[:3,3] = [0.5, 0.0, 0.76]
        
        self.table_id = self.view_objects.create_object("box")
        self.view_objects.send_prop(self.table_id, "scale",
                                    [0.61, 1.215, 0.005])
        self.view_objects.send_prop(self.table_id, "color", [0.3, 0.3, 0.3])
        self.view_objects.send_prop(self.table_id, "timeout", [-1])
        self.view_objects.send_prop(self.table_id, "pose",
                                    list(table_pose.flatten()))

    def loop(self, frequency=60.0):
        while True:
            init_time = time()
            self.pose = self.get_pose()
            if self.first:
                self.dt = 1 / 60.0
                self.previous_pose = self.pose
                self.first = False

            cmd = self.recv_cmd()
            self.update_vis(cmd)

            twist = my_diff(self.previous_pose, self.pose, self.dt)
            # Send this robot status
            self.send_pose(self.pose)
            self.send_twist(twist)

            # Move the robot
            self.move(cmd)

            self.previous_pose = self.pose

            # Handle sleep time
            elapsed_time = time() - init_time
            if 0 > (1 / 60.0 - elapsed_time):
                print("RobotController: Cycle too slow")
            else:
                sleep(1 / 60.0 - elapsed_time)

    def get_pose(self):
        try:
            pose = array(self.harm.getPose(blocking=False))
        except AttributeError:
            pose = self.pose
        return pose.reshape(4, 4)

    def send_pose(self, pose):
        btl = self.pose_port.prepare()
        btl.clear()
        map(btl.addDouble, pose.flatten())
        self.pose_port.write()

    def send_twist(self, twist):
        btl = self.twist_port.prepare()
        btl.clear()
        map(btl.addDouble, twist)
        self.twist_port.write()

    def recv_cmd(self, period=1.0/6.0):
        if not self.ignore_oms:
            btl = self.cmd_port.read(False)
            if btl:
                cmd = array(
                    map(yarp.Value.asDouble, map(btl.get, range(btl.size()))))
                return cmd.reshape(4, 4)
            else:
                return self.pose
        else:
            cmd = my_adddelta(self.pose, self.vel, period)
            chars = self.raw_key.get_num_chars()
            if not len(chars) > 0:
                return cmd

            if chars == Keys.UP_ARROW:
                self.vel[1] += self.acceleration
            if chars == Keys.DOWN_ARROW:
                self.vel[1] -= self.acceleration
            if chars == Keys.RIGHT_ARROW:
                self.vel[0] += self.acceleration
            if chars == Keys.LEFT_ARROW:
                self.vel[0] -= self.acceleration

            if chars == Keys.o:
                self.vel[2] += self.acceleration
            if chars == Keys.l:
                self.vel[2] -= self.acceleration

            if chars == Keys.d:
                self.vel[5] += self.acceleration * 5
            if chars == Keys.a:
                self.vel[5] -= self.acceleration * 5
            if chars == Keys.w:
                self.vel[3] += self.acceleration * 5
            if chars == Keys.s:
                self.vel[3] -= self.acceleration * 5

            self.vel *= 1 - self.damping
            return cmd

    def move(self, cmd):
        """
        cmd is the pose to where I have to move
        """
        # Only send updates if the arm has to move
        if list(self.pose.flatten()) != list(cmd.flatten()):
            move_robot(self.hand, self.push_finger, self.harm, cmd,
                       [0.03, 5.0 * pi / 180.0], wait=1.0/60.0)

    def update_vis(self, cmd):
        self.view_objects.send_prop(self.goal_id, "pose", list(cmd.flatten()))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-g",
        "--ignore-oms",
        dest="ignore_oms",
        default=False,
        action="store_true",
        help="Ignore OMS"
    )
    parser.add_argument(
        "-n",
        "--namespace",
        dest="namespace",
        default="",
        help="define the namespace"
    )

    parser.add_argument(
        "-a",
        "--acceleration",
        dest="acceleration",
        default=0.01,
        type=float,
        help="Define the velocity increments with each key stroke"
    )
    parser.add_argument(
        "-d",
        "--damping",
        dest="damping",
        default=0.0,
        type=float,
        help="Define a damping index for soft-stopping the robot"
    )
    args = parser.parse_args()
    robot_controller = RobotController(ignore_oms=args.ignore_oms,
                                       namespace=args.namespace,
                                       acceleration=args.acceleration,
                                       damping=args.damping)
    robot_controller.initial_configuration()
    robot_controller.loop()


if __name__ == "__main__":
    main()
