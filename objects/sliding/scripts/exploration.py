#!/usr/bin/python
# Copyright (c) 2011 Technische Universitaet Muenchen, Informatik Lehrstuhl IX.
# Author: Federico Ruiz-Ugalde
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from cmoc.objects.sliding.robot_params import force_factor
from cmoc.objects.sliding import LS
import sys, time
#sys.path.append("../../control/motionControl")
#sys.path.append("../../tools/python/numeric/")
#sys.path.append("../../robots/kimp/hand/hand_cartesian/")
#sys.path.append("../../tools/python/computer_graphics")
#sys.path.append("../../perception/webcam_baseframe_calibration/")
#sys.path.append("../../tools/python/rawkey")
from arcospyu.computer_graphics.pluecker_test import polygon_pluecker_test, triangle_ray_intersection, plane_ray_intersection
from numpy import array, identity, dot, concatenate, sign, cross, arccos, pi, arctan, tan, cos, sin, sign, exp, arctan2, mean, std
from numpy.linalg import pinv, inv, norm
import PyKDL as kdl
import yarp
from arcospyu.yarp_tools.yarp_comm_helpers import new_port, readListPort, write_narray_port, yarpListToList, bottle_to_list
#from helpers import Roboviewer_objects, Joint_sim, Finger_sim, Hand_sim, Controlloop, kdlframe_to_narray, narray_to_kdlframe, narray_to_kdltwist, kdltwist_to_narray, rot_vector_angle, my_adddelta, my_get_euler_zyx
#from rawkey import Raw_key, Keys
from scipy.signal import iirfilter, lfilter, lfiltic
from arcospyu.numeric import quat
from cmoc.robot.hand_sim_handler import Hand
from cmoc.robot.sim_handlers import Force_handle
from cmoc.objects.sliding.utils import Object_pos_handle, wait_valid_object_pose, move_robot, finger_joint_move, move_robot_fast, find_force_noise_levels
from arcospyu.signal_proc.filters import  Filter_vector, Filter
from arcospyu.robot_tools.robot_trans import rot_y, rot_z, homo_matrix
from arcospyu.numeric.lin_alg import angle_from_a_to_b, vector_saturation2
from arcospyu.computer_graphics.ray_tracing import find_crossing_faces, most_far_away_face_from_point
from cmoc.objects.sliding.box_control import calc_finger_start_pos, calc_finger_orient, approach_until_touch
from cmoc.objects.sliding.box import forces_to_no_map, force_c_to_force_o_map, vc_to_vo_linear_model, fc_to_nc_matrix, nc_to_forces_map, vo_to_vc
import pickle
from numpy import max as npmax
from numpy import mean as npmean

# class Filter():
#     def __init__(self, order=2, freq=0.7,y=[],x=[]):
#          self.b,self.a=iirfilter(order, freq, btype="lowpass")
#          if len(y)>0:
#              print "here"
#              self.z=lfiltic(self.b,self.a, y, x=x)
#          else:
#              self.z=array([0.]*order)
#          #self.z=lfiltic(self.b,self.a, y,x=x)
    
#     def filter(self,raw_data):
#         #print "Raw", array(raw_data)
#         y,self.z=lfilter(self.b,self.a,raw_data,zi=self.z, axis=0)
#         return(y)


def approach_until_spike(hand_handle, finger, arm_handle, force_handle, final_point, force_threshold, goal_precision, speed, orient, force_hysteresis, speed_force_threshold, object_handle, sim, weight_force, extra_tool=identity(4)):
    #moves until it touches object. Then it measures the peak, it waits for new peaks as long as the force value doesn't decrease by more than a threshold. Afterwards it looks for the dynamic friction coef.
    friction_filename="friction_data"+"_"+time.strftime("%d.%m.%Y_%H.%M.%S")+".dat"
    friction_file=open(friction_filename,"wb")
    store_time=[]
    store_finger_data=[]
    store_force_loc=[]
    store_force_loc_filter=[]
    store_cur_pose=[]
    store_force_filter=[]
    store_force_speed_filter=[]
    store_cur_friction_coef=[]
    store_cmd_pose=[]
    store_event=[]

    dynamic_wait=3.
    get_dynamic=False

    filters_order=3
    filters_freq=0.6
    max_noise_force, max_noise_norm_force=find_force_noise_levels(force_handle, finger, filter_order=filters_order, filter_freq=filters_freq, measurements=100)
    print "noise levels", max_noise_force, max_noise_norm_force
    filters_order=3
    filters_freq=0.6
    #raw_input()
    #if False:
    if not sim:
        force_threshold=max_noise_norm_force*4.2 #for detecting touch
        force_hysteresis=max_noise_norm_force*1.1 #for stopping looking for new peaks
        force_hysteresis=0.55

    force=array([0.]*3)
    init_time=time.time()
    old_time=init_time
    first=True
    force_filter=Filter(order=filters_order, freq=filters_freq)
    force_loc_filter=Filter_vector(dim=3, order=filters_order, freq=filters_freq)
    force_speed_filter=Filter(order=filters_order, freq=filters_freq)
    norm_force=norm(force)
    cont=True
    touched=False
    peaked=False
    peaked_once=False
    peak_force=force
    speed_force=0.
    cur_time=time.time()
    old_time=cur_time
    speed_adjust=3.
    peak_time_out=2.
    while cont:
        cur_time=time.time()
        force_handle.update_data()
        xo=object_handle.get_object_pos()
        data=force_handle.get_data_finger(finger, update=False)
        period=cur_time-old_time
        old_time=cur_time
        force=data[-3:]*force_factor
        force_loc=dot(inv(xo[:3,:3]), force)
        old_norm_force=norm_force
        force_loc_filtered=force_loc_filter.filter(force_loc)
        friction_coef=force_loc_filtered[0]/(weight_force+force_loc_filtered[2])
        norm_force=norm(force_loc_filtered)
        #norm_force=force_filter.filter(array([norm_force]))[0]
        speed_force=norm(force_speed_filter.filter(array([norm_force-old_norm_force])))
        #print "Force", norm_force, "Speed", speed_force
        cur_pose=force_handle.get_pose(finger, update=False, extra_tool=extra_tool)
        store_time.append(cur_time)
        store_finger_data.append(data)
        store_force_loc.append(force_loc)
        store_force_loc_filter.append(force_loc_filtered)
        store_cur_pose.append(cur_pose)
        store_force_filter.append(norm_force)
        store_force_speed_filter.append(speed_force)
        if touched:
            store_cur_friction_coef.append(friction_coef)
        else:
            store_cur_friction_coef.append(0.)

        if first:
            #cur_pose=quat.to_matrix_fixed(data[3:7], r=data[:3])
            vel_vector=(final_point[:2]-cur_pose[:2,3])
            vel_vector/=norm(vel_vector)
            pose=cur_pose
            first=False
            update_finger_cart=True
        else:
            update_finger_cart=False
        step=vel_vector*speed*period*speed_adjust
        pose[:2,3]+=step
        pose[:3,:3]=orient
        move_robot_fast(hand_handle, finger, arm_handle, pose, goal_precision=[0.1, 10*pi/180.0], extra_tool=extra_tool, update_finger_cart=update_finger_cart)
        store_cmd_pose.append(pose)
        #print "Pos", cur_pose, "Force", force
        if not touched:
            if not (norm_force<force_threshold): #not touching yet
                print "Touch happend"
                touched=True
                #speed_adjust=1. #changing speed generates undesired efects like inertial forces
                store_event.append([cur_time,"touched"])
        else:
            if norm_force>old_norm_force: #new peak force
                if norm_force>norm(peak_force):
                    print "New peak"
                    peak_force=force_loc_filtered
                    init_peak_time=cur_time
                    peaked_once=True
            elif norm_force<(norm(peak_force)-force_hysteresis): #last peak is considerably higher than current force. Stop looking for new peaks
                peaked=True
                print "No more peaks found, hysteresis"
                store_event.append([cur_time,"No more peakes: hysteresis"])
            elif peaked_once:
                if cur_time-init_peak_time>peak_time_out:
                    print "No more peaks because of timeout"
                    peaked=True
                    store_event.append([cur_time,"No more peakes: timeout"])
        if peaked and (not get_dynamic):
            print "Speed force", speed_force
            #looking for low derivative (constant value)
            if speed_force<speed_force_threshold:
                #dynamic_friction_force=norm_force
                print "Measuring dynamic"
                store_event.append([cur_time,"measuring dynamic"])
                get_dynamic=True
                #cont=False
                dynamic_init_time=time.time()
                final_dynamic_force=array([0.]*3)
                dynamic_counter=0
        if get_dynamic:
            if (time.time()-dynamic_init_time)> dynamic_wait:
                print "Stop dynamic"
                cont=False
                final_dynamic_force/=dynamic_counter
            else:
                final_dynamic_force+=force_loc_filtered
                dynamic_counter+=1

    #stop robot
    cur_pose=force_handle.get_pose(finger, update=False, extra_tool=extra_tool)
    #cur_pose=quat.to_matrix_fixed(data[3:7], r=data[:3])
    move_robot_fast(hand_handle, finger, arm_handle, cur_pose, goal_precision=[0.03, 10*pi/180.0], extra_tool=extra_tool)
    print "Static friction force", peak_force
    print "Dynamic friction force", final_dynamic_force
    store={
        "name": "push for friction",
        "time": store_time,
        "finger_data": store_finger_data,
        "force_loc": store_force_loc,
        "force_loc_filter": store_force_loc_filter,
        "cur_pose": store_cur_pose,
        "force_filter": store_force_filter,
        "force_speed_filter": store_force_speed_filter,
        "cur_friction_coef": store_cur_friction_coef,
        "cmd_pose": store_cmd_pose,
        "events": store_event,
        "peak_force": peak_force,
        "dynamic_friction_force": final_dynamic_force
        }
    pickle.dump(store, friction_file)
    friction_file.close()
    return(peak_force, final_dynamic_force)




def approach_until_spike_object_friction(normal_force, hand_handle, finger, arm_handle, force_handle, force_threshold, goal_precision, speed, orient, force_hysteresis, speed_force_threshold, object_handle, sim, extra_tool=identity(4)):
    #moves until it touches object with a particular force. Then it measures the peak, it waits for new peaks as long as the force value doesn't decrease by more than a threshold. Afterwards it looks for the dynamic friction coef.
    initial_speed=float(speed)
    friction_filename="friction_finger_object_data"+"_"+time.strftime("%d.%m.%Y_%H.%M.%S")+".dat"
    friction_file=open(friction_filename,"wb")
    store_time=[]
    store_finger_data=[]
    store_force=[]
    store_force_filter=[]
    store_norm_planar_force=[]
    store_cur_friction_coef=[]
    store_cur_friction_coef_speed=[]
    store_cur_pose=[]
    store_force_speed_filter=[]
    store_cmd_pose=[]
    store_event=[]

    filters_order=3
    filters_freq=0.6
    max_noise_force, max_noise_norm_force=find_force_noise_levels(force_handle, finger, filter_order=filters_order, filter_freq=filters_freq, measurements=50)
    print "noise levels", max_noise_force, max_noise_norm_force
    #raw_input()
    filters_order=3
    filters_freq=0.6
    if not sim:
        force_threshold=max_noise_norm_force*1.5 #for detecting touch
        force_hysteresis=max_noise_norm_force*3.5 #for stopping looking for new peaks
        force_hysteresis=0.55

    # force=array([0.]*3)
    # init_time=time.time()
    # old_time=init_time
    # #force_filter=Filter(order=filters_order, freq=filters_freq)
    # norm_force=norm(force)
    # touched=False
    peaked=False
    # peak_force=force

    cont=True
    speed_force=0.
    cur_time=time.time()
    old_time=cur_time
    state="look_normal_force"
    force_filter=Filter_vector(dim=3, order=filters_order, freq=filters_freq)
    force_speed_filter=Filter(order=filters_order, freq=filters_freq)
    vel_vector=array([0.,0.,-1.])
    speed_adjust=3.
    peak_planar_force=0.
    first=True
    dynamic_wait=3.
    update_cmd_pose=False
    peak_time_out=3.
    while cont:
        #raw_input()
        #print
        #get force and filter it
        cur_time=time.time()
        force_handle.update_data()
        data=-force_handle.get_data_finger(finger, update=False)
        period=cur_time-old_time
        old_time=cur_time
        force=data[-3:]*force_factor
        force_filtered=force_filter.filter(force)
        norm_planar_force=norm(force_filtered[:2])
        friction_coef=norm_planar_force/force_filtered[2]
        #print "cur friction coef", friction_coef
        if first:
            old_friction_coef=friction_coef
        friction_coef_speed=abs(friction_coef-old_friction_coef)
        old_friction_coef=friction_coef

        norm_force=norm(force_filtered)
        if first:
            old_norm_force=norm_force
        #norm_force=force_filter.filter(array([norm_force]))[0]
        speed_force=norm(force_speed_filter.filter(array([abs(norm_force-old_norm_force)])))
        old_norm_force=norm_force
        #print "Planar Force", norm_planar_force, "z force", force_filtered[2], "Speed", speed_force

        #get finger current position and log data
        pose=force_handle.get_pose(finger, update=False, extra_tool=extra_tool)
        store_time.append(cur_time)
        store_finger_data.append(data)
        store_force.append(force)
        store_force_filter.append(force_filtered)
        store_norm_planar_force.append(norm_planar_force)
        if state!="look_normal_force":
            store_cur_friction_coef.append(friction_coef)
            store_cur_friction_coef_speed.append(friction_coef_speed)
        else:
            store_cur_friction_coef.append(0.)
            store_cur_friction_coef_speed.append(0.)
        store_cur_pose.append(pose)
        store_force_speed_filter.append(speed_force)

        #position speed
        if first or update_cmd_pose:
            cmd_pose=array(pose)
        if not sim:
            step=vel_vector*speed*period*speed_adjust
        else:
            step=vel_vector*speed*speed_adjust*0.01
        cmd_pose[:3,3]+=step
        cmd_pose[:3,:3]=orient
        #print "step", step, "pose", cmd_pose
        if first:
            update_finger_cart=True
        else:
            update_finger_cart=False
        move_robot_fast(hand_handle, finger, arm_handle, cmd_pose, goal_precision=[0.1, 10*pi/180.0], extra_tool=extra_tool, update_finger_cart=update_finger_cart)
        store_cmd_pose.append(cmd_pose)
        if state=="look_normal_force":
            print "Filtered force: ", force_filtered
            if abs(force_filtered[2])>normal_force:
                store_event.append([cur_time,"normal_force_reached"])
                print "Stop moving, finger pushing object down"
                #pose=force_handle.get_pose(finger, update=False, extra_tool=extra_tool)
                #move_robot_fast(hand_handle, finger, arm_handle, pose, goal_precision=[0.001, 10*pi/180.0], extra_tool=extra_tool)
                speed=0.
                state="wait_2_seconds"
                init_time_wait=cur_time
                print "state", state
                #raw_input()
        elif state=="wait_2_seconds":
            print "Waiting 2 seconds"
            store_event.append([cur_time,"waiting for 2 seconds"])
            if cur_time> init_time_wait+2.:
                state="move_sideways"
                print "state", state
                #raw_input()
        elif state=="move_sideways":
            store_event.append([cur_time,"moving sideways"])
            print "moving sideways"
            #raw_input()
            #update_cmd_pose=True
            vel_vector=array([0., -1., 0.])  ### maybe better in "x"
            state="wait_contrary_force"
            print "state", state
            speed=initial_speed
            old_norm_planar_force=norm_planar_force
        elif state=="wait_contrary_force":
            expected_force_dir=vel_vector*vel_vector
            force_dir=sign((expected_force_dir*(force_filtered)).sum())
            print "Force dir", force_dir, "expected", expected_force_dir
            if force_dir==sign(expected_force_dir.sum()):
                print "Force now in the right direction"
                #raw_input()
                state="look_peak"
                print "state", state
                old_norm_planar_force=norm_planar_force
        elif state=="look_peak": #looks for a peak in x,y force
            #print "state", state
            update_cmd_pose=False
            store_event.append([cur_time,"look peak"])
            if norm_planar_force>old_norm_planar_force:
                if norm_planar_force>peak_planar_force:
                    store_event.append([cur_time,"new peak"])
                    print "New peak", cur_time, norm_planar_force
                    print "state", state
                    #raw_input()
                    peak_planar_force=norm_planar_force
                    peak_force=force_filtered
                    init_peak_time=cur_time
                    peaked=True
            elif norm_planar_force<(peak_planar_force-force_hysteresis):
                #last peak is considerably higher than current force. Stop looking for new peaks
                store_event.append([cur_time,"stopped looking for new peaks, hysteresis"])
                state="wait_stable_force"
                print "No more peaks", norm_planar_force, peak_planar_force, force_hysteresis
                print "state", state
                #raw_input("no more peaks")
            elif peaked:
                #print "Peaked"
                #print "state", state
                if cur_time-init_peak_time>peak_time_out:
                    print "No more peak because of timeout"
                    state="wait_stable_force"
                    print "state", state, init_peak_time, cur_time, peak_time_out
                    store_event.append([cur_time,"stopped looking for new peaks, timeout"])
            old_norm_planar_force=norm_planar_force
        elif state=="wait_stable_force":
            print "Speed force", speed_force, "threshold", speed_force_threshold
            if speed_force<speed_force_threshold:
                print "force speed low enough, ready for reading dynamic friction coef"
                store_event.append([cur_time,"force stable"])
                state="measure_dynamic_friction"
                dynamic_init_time=time.time()
                print "state", state
                final_dynamic_force=array([0.]*3)
                dynamic_counter=0
        elif state=="measure_dynamic_friction":
            store_event.append([cur_time,"measuring dynamic friction"])
            if (time.time()-dynamic_init_time)> dynamic_wait:
                store_event.append([cur_time,"stop dynamic friction"])
                cont=False
                final_dynamic_force/=dynamic_counter
                print "Stop dynamic", norm(final_dynamic_force[:2])
            else:
                final_dynamic_force+=force_filtered
                dynamic_counter+=1
        first=False

    #stop robot
    cur_pose=force_handle.get_pose(finger, update=False, extra_tool=extra_tool)
    #cur_pose=quat.to_matrix_fixed(data[3:7], r=data[:3])
    move_robot_fast(hand_handle, finger, arm_handle, cur_pose, goal_precision=[0.03, 10*pi/180.0], extra_tool=extra_tool)
    print "Peak force", peak_force, "peak planar force", peak_planar_force
    print "Dynamic friction force", final_dynamic_force
    static_coef=abs(peak_planar_force/peak_force[2])
    print "static friction coefficient", static_coef
    dynamic_coef=abs(norm(final_dynamic_force[:2])/final_dynamic_force[2])
    print "Dynamic friction coefficient", dynamic_coef
    store={
        "name": "finger object friction",
        "time": store_time,
        "finger_data": store_finger_data,
        "force": store_force,
        "force_filter": store_force_filter,
        "norm_planar_force": store_norm_planar_force,
        "cur_friction_coef": store_cur_friction_coef,
        "cur_friction_coef_speed": store_cur_friction_coef_speed,
        "cur_pose": store_cur_pose,
        "force_speed_filter": store_force_speed_filter,
        "cmd_pose": store_cmd_pose,
        "events": store_event,
        "peak_force": peak_force,
        "peak_planar_force": peak_planar_force,
        "dynamic_friction_force": final_dynamic_force,
        "static_friction_coef": static_coef,
        "dynamic_friction_coef": dynamic_coef
        }
    pickle.dump(store, friction_file)
    friction_file.close()
    return(peak_force, final_dynamic_force)

def approach_until_tilt(object_handle, hand_handle, finger, arm_handle, force_handle, final_point, force_threshold, goal_precision, speed, orient, max_tilt_angle, sim, start_pose_loc, extra_tool=identity(4)):
    #moves until it touches object. Then it moves more until object inclination angle is 2 degrees, then it stops and measures the force. It uses this force to calculate the weight
    weight_filename="weight_data"+"_"+time.strftime("%d.%m.%Y_%H.%M.%S")+".dat"
    weight_file=open(weight_filename,"wb")
    store_time=[]
    store_finger_data=[]
    store_force_loc=[]
    store_cur_pose=[]
    store_force_filter=[]
    store_force_speed_filter=[]
    store_cmd_pose=[]
    store_event=[]

    final_force=array([0.]*3)
    force=array([0.]*3)
    init_time=time.time()
    old_time=init_time
    first=True
    force_filter=Filter()
    force_speed_filter=Filter()
    norm_force=norm(force)
    cont=True
    peaked=False
    peak_force=norm_force
    speed_force=0.
    speed_adjust=2.
    z_axis_ref=array([0.]*3)
    object_pos_observations=10
    for i in xrange(object_pos_observations):
        xo=object_handle.get_object_pos()
        z_axis_ref+=xo[:3,2]
    z_axis_ref/=object_pos_observations
    z_axis_ref/=norm(z_axis_ref)
    print "object Z axis", z_axis_ref
    #raw_input()
    tilt_angle_filter=Filter(y=array([1.]*2), x=array([1.]*2))
    cur_time=time.time()
    old_time=cur_time
    z_pos_data=[]
    measure=False
    measures=0
    max_measures=50
    touched=True
    tilt_angle=0.
    while cont:
        cur_time=time.time()
        #getting force, force speed and finger pose data
        force_handle.update_data()
        data=force_handle.get_data_finger(finger, update=False)
        xo=object_handle.get_object_pos()
        period=cur_time-old_time
        old_time=cur_time
        force=data[-3:]*force_factor
        force_loc=dot(inv(xo[:3,:3]), force)
        old_norm_force=norm_force
        norm_force=norm(force_loc)
        norm_force=force_filter.filter(array([norm_force]))[0]
        speed_force=norm(force_speed_filter.filter(array([norm_force-old_norm_force])))
        print "Force", force_loc
        cur_pose=force_handle.get_pose(finger, update=False, extra_tool=extra_tool)
        #storing data
        store_time.append(cur_time)
        store_finger_data.append(data)
        store_force_loc.append(force_loc)
        store_cur_pose.append(cur_pose)
        store_force_filter.append(norm_force)
        store_force_speed_filter.append(speed_force)
        if first:
            final_z_pos=(cur_pose[2,3]-xo[2,3])
            #cur_pose=quat.to_matrix_fixed(data[3:7], r=data[:3])
            vel_vector=(final_point[:2]-cur_pose[:2,3])
            vel_vector/=norm(vel_vector)
            pose=cur_pose
            first=False
            update_finger_cart=True
        else:
            update_finger_cart=False

        #Calculating next position to move finger to give a constant speed
        if not measure:
            step=vel_vector*speed*period*speed_adjust
            pose[:2,3]+=step
            pose[:3,:3]=orient
            move_robot_fast(hand_handle, finger, arm_handle, pose, goal_precision=[0.1, 10*pi/180.0], extra_tool=extra_tool,update_finger_cart=update_finger_cart)
            store_cmd_pose.append(pose)
            stopping_pose=pose

        #print "Pos", cur_pose, "Force", force
        if not touched:
            if not (norm_force<force_threshold): #not touching yet
                print "Touch happend"
                store_event.append([cur_time, "touched"])
                touched=True
                #speed_adjust=1. #changing speed creates oscillations and inertial forces
                tilt_angle=0.
            #touched, now we have to look for tilting
        elif touched:
            if not measure:
                #xo=object_handle.get_object_pos()
                z_axis=xo[:3,2]
                print "Z axis", z_axis, z_axis_ref
                tilt_angle_cos=tilt_angle_filter.filter(array([dot(z_axis,z_axis_ref)]))
                if not sim:
                    tilt_angle=arccos(min(tilt_angle_cos[0],1.))
                else:
                    tilt_angle+=0.01
                print "Tilt angle", tilt_angle*180.0/pi, tilt_angle_cos, dot(z_axis, z_axis_ref)
                if tilt_angle>max_tilt_angle:
                    store_event.append([cur_time, "tilted"])
                    print "Measuring force"
                    #cont=False
                    measure=True
                    #final_force=norm_force
                    #final_z_pos=(cur_pose[2,3]-xo[2,3])
                    #stop robot
                    ##cur_pose=force_handle.get_pose(finger, update=False, extra_tool=extra_tool)
                    ##move_robot_fast(hand_handle, finger, arm_handle, cur_pose, goal_precision=[0.03, 10*pi/180.0], extra_tool=extra_tool)
                    pose=cur_pose
                    pose[:3,:3]=orient
                    #move_robot_fast(hand_handle, finger, arm_handle, pose, goal_precision=[0.1, 10*pi/180.0], extra_tool=extra_tool,update_finger_cart=update_finger_cart)
                    move_robot_fast(hand_handle, finger, arm_handle, stopping_pose, goal_precision=[0.1, 10*pi/180.0], extra_tool=extra_tool,update_finger_cart=update_finger_cart)
                    store_cmd_pose.append(pose)
        if measure:
            print "Force loc", force_loc
            final_force+=force_loc
            measures+=1
            if measures==max_measures:
                cont=False
                final_force/=max_measures
                #final_z_pos=(cur_pose[2,3]-xo[2,3])

    print "Weight force", final_force
    #final_z_pos+=0.023
    final_z_pos+=0.01
    store={
        "name": "push for tilt",
        "z_axis_ref": z_axis_ref,
        "time": store_time,
        "finger_data": store_finger_data,
        "force_loc": store_force_loc,
        "cur_pose": store_cur_pose,
        "force_filter": store_force_filter,
        "force_speed_filter": store_force_speed_filter,
        "cmd_pose": store_cmd_pose,
        "events": store_event,
        "push_force": final_force,
        "final_z_pos": final_z_pos
        }
    pickle.dump(store, weight_file)
    weight_file.close()
    return(final_force, final_z_pos)

def vector_saturation(vector_min, vector_max, vector):
    angle_min_max=arccos(dot(vector_min,vector_max)/(norm(vector_min)*norm(vector_max)))
    angle_max=arccos(dot(vector,vector_max)/(norm(vector)*norm(vector_max)))
    angle_min=arccos(dot(vector,vector_min)/(norm(vector)*norm(vector_min)))
    print "angles", angle_min_max, angle_max, angle_min
    if (angle_max > angle_min_max) or (angle_min > angle_min_max):
        print "limiting"
        if angle_max < angle_min:
            return(vector_max)
        else:
            return(vector_min)
    return(vector)

def read_finger(yarp_port, blocking, finger):
    result=yarp_port.read(blocking)
    bottle=result.get(finger).asList()
    #print "Result", bottle
    trans=array(map(yarp.Value.asDouble,map(bottle.get, range(3))))
    rot=array(map(yarp.Value.asDouble,map(bottle.get, range(3,7))))
    force=array(map(yarp.Value.asDouble,map(bottle.get, range(7,10))))
    return(trans,rot,force)



class Calib(object):

    def find_finger_object_friction(self):

        print "Finding finger-object friction"
        peak_forces=[]
        dynamic_friction_forces=[]
        table_height=0.75
        object_height=table_height+min(min(self.obj_par.box_dim[0],self.obj_par.box_dim[1]), self.obj_par.box_dim[2])
        object_pose=identity(4)
        object_pose[:3,:3]=array([[0., -1., 0.], #orientation finger looking to table
                                  [-1., 0., 0.],
                                  [0., 0., -1.]])
        start_distance=0.02
        for i in xrange(self.repetitions):
            #updating table height_based on last measured object pose (from touching)
            object_pose[:3,3]=array([0.7, 0., object_height])

            #raw_input("Start arm position")
            #Send arm to away for looking for object position.
            max_stiffness_arm=array([40.0]*7)
            #max_stiffness_arm=array([240.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)
            self.harm_bridge.joint_controller()
            self.harm_joint.set_ref_js(self.cam_away_joint_pos["right"],wait=0.5,goal_precision=[0.1]*7)
            #raw_input("Put object in scene. Check that it gets correctly detect")
            #max_stiffness_arm=array([40.0]*7)
            max_stiffness_arm=array([140.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)
            time.sleep(1)
            max_stiffness_arm=array([240.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)


            #move finger to start position
            #pose_to_robot=dot(self.start_pose,inv(self.rel_pose_marker_finger))
            start_pose=array(object_pose)
            start_pose[2,3]+=start_distance
            self.harm_bridge.cartesian_controller()
            #move_robot(self.hand_right_server, self.finger, self.harm, pose_to_robot, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            move_robot(self.hand_right_server, self.finger, self.harm, start_pose, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            time.sleep(1)

            #Zero finger forces
            self.hand_right_server.calibrate()



            #Approach object in straight line while looking for friction forces (spike and then stable)
            force_threshold=0.5 #when we start looking for the spike
            force_hysteresis=0.1 # this is used to stop looking for a new spike
            speed_force_threshold=0.01 #speed for deciding the signal is stable (to measure dynamic friction coeficient)
            finger_approach_speed=0.001
            print "Iteration: ", i
            raw_input("Press enter to start approach")
            normal_force=8.0 #one newton : This is the force to press the object against the
            #table to later measure the finger-object friction coefficient
            peak_force, dynamic_friction_force=approach_until_spike_object_friction(normal_force, self.hand_right_server, self.finger, self.harm, self.hforce, force_threshold, self.goal_precision, finger_approach_speed, array(start_pose[:3,:3]), force_hysteresis, speed_force_threshold, self.object_pos_handle, self.simulation,  self.rel_pose_marker_finger)
            peak_forces.append(peak_force)
            dynamic_friction_forces.append(dynamic_friction_force)

            #move finger to start position
            self.harm_bridge.cartesian_controller()
            #move_robot(self.hand_right_server, self.finger, self.harm, pose_to_robot, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            move_robot(self.hand_right_server, self.finger, self.harm, start_pose, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            time.sleep(1)

        mean_peak_force=npmean(array(peak_forces), axis=0)
        mean_dynamic_force=npmean(array(dynamic_friction_forces), axis=0)
        print "Peak force mean", mean_peak_force
        print "Dynamic friction force mean", mean_dynamic_force

        static_fric_coefs=[]
        for peak_force in peak_forces:
            static_fric_coefs.append(norm(peak_force[:2])/peak_force[2]) # x push force divided against (weight force+ z push force)
        dyn_fric_coefs=[]
        for dynamic_friction_force in dynamic_friction_forces:
            dyn_fric_coefs.append(norm(dynamic_friction_force[:2])/dynamic_friction_force[2]) # x push force divided against (weight force+ z push force)

        static_friction_coef=mean(static_fric_coefs)
        dynamic_friction_coef=mean(dyn_fric_coefs)
        static_friction_coef_dev=std(static_fric_coefs)
        dynamic_friction_coef_dev=std(dyn_fric_coefs)

        print "Static friction coefficient finger-object", static_friction_coef
        print "Dynamic friction coefficient finger-object", dynamic_friction_coef
        print "Static friction coefficient std dev finger-object", static_friction_coef_dev
        print "Dynamic friction coefficient std dev finger-object", dynamic_friction_coef_dev
        return(peak_forces, dynamic_friction_forces, mean_peak_force, mean_dynamic_force, static_friction_coef, dynamic_friction_coef, static_friction_coef_dev, dynamic_friction_coef_dev )

    def calib(self, simulation, rel_pose_marker_finger, object_params, arcoslab_namespace, config_hands, fcd):
        weight_repetitions=1
        friction_repetitions=1
        self.repetitions=10

        self.simulation=simulation
        print "Simulation", self.simulation
        if self.simulation:
            self.rel_pose_marker_finger=identity(4)
        else:
        #if True:
            self.rel_pose_marker_finger=rel_pose_marker_finger
        #self.rel_pose_marker_finger=rel_pose_marker_finger
        #raw_input()

        self.obj_par=object_params.Object_params()

        self.finger_object_face=1
        self.table_object_face=5

        self.box_sides_angle=arctan(self.obj_par.box_dim[1]/self.obj_par.box_dim[0])

        # yarp ports
        base_name="/parameter_explore"

        #Arm/finger control initialization.
        from vfclik.handlers import HandleArm, HandleJController, HandleBridge
        arm='right'
        hand='right'
        robot='lwr'
        fingers=[0,1,2,3]
        arm_portbasename="/"+robot+"/"+arm

        self.finger=3 #ring finger right hand
        finger_initial_pos=array([0.,6.,6.]) #angles in degrees
        thumb_finger_initial_pos=array([0.,0.,15.,15.]) #angles in degrees
        thumb_finger_pushing_pos=array([0.,0.,10.,10.]) #angles in degrees
        thumb_finger_pushing_pos = array([0.0, 0.03322, 10.0, 7.74433])
        thumb_finger_pushing_pos = array([0.0, 0.0, 50.0, 20.0])
        index_finger_pushing_pos=array([20.,7.,8.]) #angles in degrees
        middle_finger_pushing_pos=array([20.,7.,8.]) #angles in degrees
        finger_pushing_pos=array([0.,15.,35]) #angles in degrees

        initial_pose={
            'right': array([[-1., 0., 0., 0.8],
                            [0., 0., 1., -0.1],
                            [0., 1., 0., 1.14],
                            [0., 0., 0., 1.]]),
            'left': array([[-1., 0., 0., 0.74],
                           [0., 0., 1., -0.1],
                           [0., 1., 0., 1.14],
                           [0., 0., 0., 1.]])
            }

        initial_pose={ #ARCOS-Lab INII 2019.11.12
            'right': array([[-1., 0., 0., 0.6],
                            [0., 0., 1., -0.1],
                            [0., 1., 0., 1.14],
                            [0., 0., 0., 1.]]),
            'left': array([[-1., 0., 0., 0.74],
                           [0., 0., 1., -0.1],
                           [0., 1., 0., 1.14],
                           [0., 0., 0., 1.]])
            }
        
        base_pushing_orient=array([[0.,0.,-1.], # Initial finger orientation. Or base finger transformation
                                   [-1.,0.,0.],
                                   [0.,1.,0.]])
        #self.goal_precision=[0.01, 0.5*pi/180.0]
        self.goal_precision=[0.03, 3.0*pi/180.0]

        initial_joint_pos={
            "right": array([0, -1.2, 0.7, 1.4, 0.35, -1.4, 0]),
            "left": array([0.78, 1.6, -0.4, -1.3, 1, 0.5, 0.7])
            }

        initial_joint_pos={
            "right": array([0.0, -0.57, 0.0, 1.4, -0.7, 0.95, 1.0]) # With Andres Alvarado torso
            }


        self.cam_away_joint_pos={
            "right": array([60., -35, 45, 45, 0., -90, 0])*pi/180.,
            "left": array([0.78, 1.6, -0.4, -1.3, 1, 0.5, 0.7])
            }

        self.cam_away_joint_pos={   # With Andres Alvarado torso
            "right": array([0.0, -0.57, 0.0, 1.4, -0.7, 0.95, 1.0]),
            }

        #setting up
        self.harm=HandleArm(arcoslab_namespace+arm_portbasename,handlername=arcoslab_namespace+base_name+"arm")
        self.harm_joint=HandleJController(arcoslab_namespace+arm_portbasename,handlername=arcoslab_namespace+base_name+"arm_joint")
        self.harm_bridge=HandleBridge(arcoslab_namespace+arm_portbasename,handlername=arcoslab_namespace+base_name+"arm_bridge",torso=False)
        self.hand_right_server=Hand(config_hands, handedness="right",portprefix=base_name,sahand_number=0,arcoslab_namespace=arcoslab_namespace)
        #self.hand_left_server=Hand(handedness="left",portprefix=base_name,sahand_number=1)
        #self.hand_right_server=Hand(handedness="right",portprefix=base_name,sahand_number=1)
        #self.hand_left_server=Hand(handedness="left",portprefix=base_name,sahand_number=0)
        self.hforce=Force_handle(arcoslab_namespace+base_name, arcoslab_namespace+"/torque_sim/force_out")
        hand_calib=not self.simulation
        #hand_calib=False
        if hand_calib:
        #if False:
            print "Setting calibration offsets and factors"
            for finger,(angle_offset,torque_factor) in enumerate(zip(fcd.angle_calibration_data_right,fcd.torque_calibration_factors_right)):
                self.hand_right_server.fingers[finger].angle_offsets=angle_offset
                self.hand_right_server.fingers[finger].torque_calibration_factors=torque_factor
                print "Finger: ", finger
                print "Torque factor", self.hand_right_server.fingers[finger].torque_calibration_factors
                print "Angle offsets", (180.0/pi)*self.hand_right_server.fingers[finger].angle_offsets
                self.hand_right_server.update_angle_offsets(list_fingers=[finger])
            #for finger,(angle_offset,torque_factor) in enumerate(zip(angle_calibration_data_left,torque_calibration_factors_left)):
            #    self.hand_left_server.fingers[finger].angle_offsets=angle_offset
            #    self.hand_left_server.fingers[finger].torque_calibration_factors=torque_factor
            #    self.hand_left_server.update_angle_offsets(list_fingers=[finger])

        time.sleep(1)

        max_stiffness_finger=array([28.64,28.64,8.59])
        finger_max_speed=pi
        finger_speed=finger_max_speed/2.
        for i in fingers:
            self.hand_right_server.set_params(i,[finger_speed,finger_speed,finger_speed]+(max_stiffness_finger).tolist())
            #self.hand_left_server.set_params(i,[finger_speed,finger_speed,finger_speed]+(max_stiffness_finger).tolist())
        #self.hand_left_server.update_controller_params()
        self.hand_right_server.update_controller_params()
        #0.64 (soft for simulation) 40 (soft for real robot)
        max_stiffness_arm=array([1000.0]*7)
        max_stiffness_arm=array([40.0]*7)
        #max_stiffness_arm=array([240.0]*7)
        #self.harm.set_stiffness(max_stiffness_arm)

        print "Moving fingers"
        #Initial finger joint pos
        #finger_joint_move(self.hand_right_server, 0, thumb_finger_initial_pos, wait=-1, self.goal_precision=5.*pi/180.)
        #finger_joint_move(self.hand_right_server, self.finger, finger_initial_pos, wait=-1, self.goal_precision=5.*pi/180.)

        #Go to initial pose
        #self.harm.setTool(kdl.Frame())
        #self.harm_bridge.joint_controller()
        #self.harm_joint.set_ref_js(initial_joint_pos["right"],wait=-1,goal_precision=[0.1]*7)
        #self.harm_bridge.cartesian_controller()
        #self.harm.setTool(kdl.Frame())
        #self.harm.setTool(kdl.Frame())
        #self.harm.gotoFrame(initial_pose[arm].reshape(16),wait=-1,goal_precision=self.goal_precision)
        print "Initial position ready"

        #Pushing finger joint pos
        print "Pushing finger position"
        print "Moving thumb"
        #raw_input()
        finger_joint_move(self.hand_right_server, 0, thumb_finger_pushing_pos, wait=-1, goal_precision=5.)
        #raw_input("Moving index and middle fingers away")
        finger_joint_move(self.hand_right_server, 1, index_finger_pushing_pos, wait=-1, goal_precision=10.)
        finger_joint_move(self.hand_right_server, 2, middle_finger_pushing_pos, wait=-1, goal_precision=10.)
        print "Moving ring"
        time.sleep(0.5)
        finger_joint_move(self.hand_right_server, self.finger, finger_pushing_pos, wait=-1, goal_precision=5.)
        #raw_input("Finished moving")
        sim_model=self.simulation # When using model simulation, otherwise object position from marker tracking
        self.object_pos_handle=Object_pos_handle(base_name, arcoslab_namespace=arcoslab_namespace, simulation=sim_model)

        gravity_force=9.81
        gravity_force=9.78 #Costa Rica


        raw_input("Tilting")
        #for calculating weight
        #weight_repetitions=self.repetitions
        normal_forces=[]
        push_forces_loc=[]
        for i in xrange(weight_repetitions):


            #Send arm to away for looking for object position.
            max_stiffness_arm=array([40.0]*7)
            #max_stiffness_arm=array([240.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)
            self.harm_bridge.joint_controller()
            self.harm_joint.set_ref_js(self.cam_away_joint_pos["right"],wait=0.5,goal_precision=[0.1]*7)
            #raw_input("Put object in scene. Check that it gets correctly detect")
            #max_stiffness_arm=array([40.0]*7)
            max_stiffness_arm=array([140.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)
            time.sleep(1)
            max_stiffness_arm=array([240.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)

            cont="n"
            while cont!="y":
                #getting current box position (TODO: make this more stable, read multiple times, wait until is stable, wait for aceptance from user)
                print "Getting initial object position"
                self.xo=self.object_pos_handle.get_object_pos()
                print "Box global pose", self.xo
                print "Iteration: ", i
                cont=raw_input("If right, press \"y\"")
                #self.box_pose=identity(4)
                #self.box_pose[:3,3]=array([3.,2,0.])

            #Send finger goal to somewhere a bit away from object.
            self.start_pose_loc=identity(4)
            self.start_pose_loc[:3,:3]=base_pushing_orient
            start_offset=0.01
            #box_z_offset=self.obj_par.box_dim[2]-0.02
            box_z_offset=self.obj_par.tilt_height
            #box_z_offset=0.0 # uncomment-for adjusting half object height
            #box_z_offset=self.obj_par.box_dim[2]/2.-0.06
            self.start_pose_loc[:3,3]=array([self.obj_par.box_dim[0]/2.+start_offset, 0., box_z_offset])
            self.start_pose=dot(self.xo, self.start_pose_loc)
            print "Start pose", self.start_pose

            #touch_face=0 #always touch face 0
            #self.finger_start_orient=calc_finger_orient(base_pushing_orient, table_normal, self.xo, touch_face)
            #print "Finger start orient", self.finger_start_orient
        

            #move intermediate position to not crash anything
            #raw_input("Press enter to get near to object")
            temp_pos=array(self.start_pose)
            temp_pos[2,3]+=0.0
            #pose_to_robot=dot(temp_pos,inv(self.rel_pose_marker_finger))
            self.harm_bridge.cartesian_controller()
            #move_robot(self.hand_right_server, self.finger, self.harm, temp_pos, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            print "Rel pose marker finger", self.rel_pose_marker_finger
            move_robot(self.hand_right_server, self.finger, self.harm, temp_pos, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            #move_robot(self.hand_right_server, self.finger, self.harm, temp_pos, self.goal_precision)

            #move finger to start position
            #raw_input("Press enter to go the start position")
            #pose_to_robot=dot(self.start_pose,inv(self.rel_pose_marker_finger))
            self.harm_bridge.cartesian_controller()
            move_robot(self.hand_right_server, self.finger, self.harm, self.start_pose, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            time.sleep(2)

            #raw_input("Press enter to start approach")
            #Zero finger forces
            print "Zeroing finger forces"
            self.hand_right_server.calibrate()
            time.sleep(2)


            #Approach object in straight line while looking for friction forces (spike and then stable)
            force_threshold=0.5 #when we start looking for the spike
            finger_approach_speed=0.003
            max_tilt_angle=self.obj_par.max_tilt_angle
            push_force_loc, final_z_pos=approach_until_tilt(self.object_pos_handle, self.hand_right_server, self.finger, self.harm, self.hforce, self.xo[:2,3], force_threshold, self.goal_precision, finger_approach_speed, self.start_pose[:3,:3], max_tilt_angle, self.simulation, self.start_pose_loc, extra_tool=self.rel_pose_marker_finger)
            print "Final push force", push_force_loc
            print "Final z pos", final_z_pos
            final_z_pos+=self.obj_par.box_dim[2]/2.
            print "Final z pos", final_z_pos
            push_torque=norm(push_force_loc[:2])*final_z_pos
            center_of_mass_vector=array([self.obj_par.box_dim[0]/2., 0., self.obj_par.box_dim[2]/2.])
            rot_com=rot_y(-self.obj_par.max_tilt_angle)
            rot_center_of_mass=dot(rot_com, center_of_mass_vector)
            new_x_projection=rot_center_of_mass[0]
            print "original com", center_of_mass_vector, "new x projection", new_x_projection
            normal_force=push_torque/(new_x_projection)-push_force_loc[2]
            print "Normal force", normal_force
            normal_forces.append(normal_force)
            push_forces_loc.append(push_force_loc)

            #move finger to start position
            self.harm_bridge.cartesian_controller()
            move_robot(self.hand_right_server, self.finger, self.harm, self.start_pose, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            time.sleep(1)

            #move intermediate position to not crash anything
            #raw_input("Safe pose")
            #pose_to_robot=dot(temp_pos,inv(self.rel_pose_marker_finger))
            self.harm_bridge.cartesian_controller()
            move_robot(self.hand_right_server, self.finger, self.harm, temp_pos, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            print "weight", normal_force/gravity_force
            #raw_input("Finished")

        print "Normal force mean", mean(normal_forces)

        #calculate weight and friction coeficients
        weight=mean(normal_forces)/gravity_force
        print "Weight", weight

        print "Finding table friction"
        #friction_repetitions=self.repetitions
        peak_forces=[]
        dynamic_friction_forces=[]
        for i in xrange(friction_repetitions):

            #raw_input("Start arm position")
            #Send arm to away for looking for object position.
            max_stiffness_arm=array([40.0]*7)
            #max_stiffness_arm=array([240.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)
            self.harm_bridge.joint_controller()
            self.harm_joint.set_ref_js(self.cam_away_joint_pos["right"],wait=0.5,goal_precision=[0.1]*7)
            #raw_input("Put object in scene. Check that it gets correctly detect")
            #max_stiffness_arm=array([40.0]*7)
            max_stiffness_arm=array([140.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)
            time.sleep(1)
            max_stiffness_arm=array([240.0]*7)
            self.harm.set_stiffness(max_stiffness_arm)

            cont="n"
            while cont!="y":
                #getting current box position (TODO: make this more stable, read multiple times, wait until is stable, wait for aceptance from user)
                print "Getting initial object position"
                self.xo=self.object_pos_handle.get_object_pos()
                print "Box global pose", self.xo
                print "Iteration: ", i
                cont=raw_input("If right, press \"y\"")
                #self.box_pose=identity(4)
                #self.box_pose[:3,3]=array([3.,2,0.])

            #Send finger goal to somewhere a bit away from object.
            self.start_pose_loc=identity(4)
            self.start_pose_loc[:3,:3]=base_pushing_orient
            start_offset=0.01
            box_z_offset=0.04
            self.start_pose_loc[:3,3]=array([self.obj_par.box_dim[0]/2.+start_offset, 0., -self.obj_par.box_dim[2]/2.+box_z_offset])
            #self.start_pose_loc[:3,3]=array([self.obj_par.box_dim[0]/2.+start_offset, 0., 0.])
            self.start_pose=dot(self.xo, self.start_pose_loc)
            print "Start pose", self.start_pose

            #touch_face=0 #always touch face 0
            #self.finger_start_orient=calc_finger_orient(base_pushing_orient, table_normal, self.xo, touch_face)
            #print "Finger start orient", self.finger_start_orient
        

            #move intermediate position to not crash anything
            #raw_input("Press enter to get near to object")
            temp_pos=array(self.start_pose)
            temp_pos[2,3]+=self.obj_par.box_dim[2]
            #pose_to_robot=dot(temp_pos,inv(self.rel_pose_marker_finger))
            #raw_input("With finger marker correction")
            self.harm_bridge.cartesian_controller()
            #move_robot(self.hand_right_server, self.finger, self.harm, pose_to_robot, self.goal_precision)
            move_robot(self.hand_right_server, self.finger, self.harm, temp_pos, self.goal_precision, extra_tool=self.rel_pose_marker_finger)

            #move finger to start position
            #raw_input("Press enter to go the start position")
            #pose_to_robot=dot(self.start_pose,inv(self.rel_pose_marker_finger))
            self.harm_bridge.cartesian_controller()
            #move_robot(self.hand_right_server, self.finger, self.harm, pose_to_robot, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            move_robot(self.hand_right_server, self.finger, self.harm, self.start_pose, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            time.sleep(1)

            #Zero finger forces
            self.hand_right_server.calibrate()

            #Approach object in straight line while looking for friction forces (spike and then stable)
            force_threshold=0.5 #when we start looking for the spike
            force_hysteresis=0.1 # this is used to stop looking for a new spike
            speed_force_threshold=0.01 #speed for deciding the signal is stable (to measure dynamic friction coeficient)
            finger_approach_speed=0.001
            #raw_input("Press enter to start approach")
            peak_force, dynamic_friction_force=approach_until_spike(self.hand_right_server, self.finger, self.harm, self.hforce, self.xo[:2,3], force_threshold, self.goal_precision, finger_approach_speed, array(self.start_pose[:3,:3]), force_hysteresis, speed_force_threshold, self.object_pos_handle, self.simulation, mean(normal_forces),  self.rel_pose_marker_finger)
            peak_forces.append(peak_force)
            dynamic_friction_forces.append(dynamic_friction_force)

            #move finger to start position
            self.harm_bridge.cartesian_controller()
            #move_robot(self.hand_right_server, self.finger, self.harm, pose_to_robot, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            move_robot(self.hand_right_server, self.finger, self.harm, self.start_pose, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            time.sleep(1)

            #move intermediate position to not crash anything
            #raw_input("Safe pose")
            #pose_to_robot=dot(temp_pos,inv(self.rel_pose_marker_finger))
            self.harm_bridge.cartesian_controller()
            #move_robot(self.hand_right_server, self.finger, self.harm, pose_to_robot, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            move_robot(self.hand_right_server, self.finger, self.harm, temp_pos, self.goal_precision, extra_tool=self.rel_pose_marker_finger)
            print "static coef", norm(peak_force[:2])/(normal_force+peak_force[2])
            print "dynamic coef", norm(dynamic_friction_force[:2])/(normal_force+dynamic_friction_force[2])
            #raw_input("Finished")

        mean_peak_force=npmean(array(peak_forces), axis=0)
        mean_dynamic_force=npmean(array(dynamic_friction_forces), axis=0)
        print "Peak force mean", mean_peak_force
        print "Dynamic friction force mean", mean_dynamic_force

        static_fric_coefs=[]
        for peak_force in peak_forces:
            static_fric_coefs.append(norm(peak_force[:2])/(normal_force+peak_force[2])) # x push force divided against (weight force+ z push force)
        dyn_fric_coefs=[]
        for dynamic_friction_force in dynamic_friction_forces:
            dyn_fric_coefs.append(norm(dynamic_friction_force[:2])/(normal_force+dynamic_friction_force[2])) # x push force divided against (weight force+ z push force)

        static_friction_coef=mean(static_fric_coefs)
        dynamic_friction_coef=mean(dyn_fric_coefs)
        static_friction_coef_dev=std(static_fric_coefs)
        dynamic_friction_coef_dev=std(dyn_fric_coefs)
        print "Object table:"
        print "Static friction coefficient", static_friction_coef
        print "Dynamic friction coefficient", dynamic_friction_coef
        print "Static friction coefficient std", static_friction_coef_dev
        print "Dynamic friction coefficient std", dynamic_friction_coef_dev
        print "Weight", weight


        raw_input("Put object under finger, and hold it, so it doesn't slide on the table")

        finger_peak_forces, finger_dynamic_friction_forces, finger_mean_peak_force, finger_mean_dynamic_force, finger_static_friction_coef, finger_dynamic_friction_coef, finger_static_friction_coef_dev, finger_dynamic_friction_coef_dev=self.find_finger_object_friction()


        print "Object finger:"
        print "Static friction coefficient", finger_static_friction_coef
        print "Dynamic friction coefficient", finger_dynamic_friction_coef
        print "Static friction coefficient std", finger_static_friction_coef_dev
        print "Dynamic friction coefficient std", finger_dynamic_friction_coef_dev
        print "Object table:"
        print "Static friction coefficient", static_friction_coef
        print "Dynamic friction coefficient", dynamic_friction_coef
        print "Static friction coefficient std", static_friction_coef_dev
        print "Dynamic friction coefficient std", dynamic_friction_coef_dev
        print "Weight", weight

        store={
            "weight_normal_forces": normal_forces,
            "weight_push_forces_loc": push_forces_loc,
            "weight": weight,
            "friction_peak_forces": peak_forces,
            "friction_dynamic_friction_forces": dynamic_friction_forces,
            "friction_mean_peak_force": mean_peak_force,
            "friction_mean_dynamic_force": mean_dynamic_force,
            "friction_static_friction_coef": static_friction_coef,
            "friction_dynamic_friction_coef": dynamic_friction_coef,
            "friction_static_friction_coef_dev": static_friction_coef_dev,
            "friction_dynamic_friction_coef_dev": dynamic_friction_coef_dev,
            "finger_friction_peak_forces": finger_peak_forces,
            "finger_friction_dynamic_friction_forces": finger_dynamic_friction_forces,
            "finger_friction_mean_peak_force": finger_mean_peak_force,
            "finger_friction_mean_dynamic_force": finger_mean_dynamic_force,
            "finger_friction_static_friction_coef": finger_static_friction_coef,
            "finger_friction_dynamic_friction_coef": finger_dynamic_friction_coef,
            "finger_friction_static_friction_coef_dev": finger_static_friction_coef_dev,
            "finger_friction_dynamic_friction_coef_dev": finger_dynamic_friction_coef_dev
            }
        exploration_filename="exploration_data"+"_"+time.strftime("%d.%m.%Y_%H.%M.%S")+".dat"
        exploration_file=open(exploration_filename,"wb")
        pickle.dump(store, exploration_file)
        exploration_file.close()

        raw_input()
        

import optparse

def main():
    parser=optparse.OptionParser("usage: %prog [options]")
    parser.add_option("-s", "--simulation", action="store_true", dest="sim", default=False,help="Simulation")
    parser.add_option("-c", "--finger_cal_data", dest="finger_cal_data_filename", default="robot_description/tum-rosie/kinematics/sahand/calibration_data/finger_calibration_data.py", type="string", help="Finger calibration data filename")
    parser.add_option("-o", "--object_params_file", dest="object_params_filename", default="cmoc/objects/sliding/objects/object_params.py", type="string", help="Object parameters file")
    parser.add_option("-f", "--config_hands", dest="config_hands", default="robot_description/tum-rosie/kinematics/sahand/hands_kin.py", type="string", help="hands config filename")
    parser.add_option("-w", "--webcam_calibration_file", dest="webcam_calibration_filename", default="robot_descriptions/arcosbot/perception/webcam_calibration_values.py", type="string", help="Webcam Calibration file")
    parser.add_option(
        "-n",
        "--namespace",
        dest="namespace",
        default="/0",
        type="string",
        help="ARCOS-Lab yarp basename")
    (options, args)= parser.parse_args(sys.argv[1:])
    from arcospyu.config_parser.config_parser import import_config
    object_params=import_config(options.object_params_filename)
    webcam_calibration_values=import_config(options.webcam_calibration_filename)
    config_hands=import_config(options.config_hands)
    fcd=import_config(options.finger_cal_data_filename)
    calib=Calib()
    calib.calib(options.sim, webcam_calibration_values.rel_pose_marker_finger, object_params, options.namespace, config_hands, fcd)

if __name__=="__main__":
    main()
